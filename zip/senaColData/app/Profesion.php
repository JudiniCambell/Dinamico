<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
  protected $table="Profesion";
  protected $primaryKey="idProfesion";
  public $timestamps=false;
}
