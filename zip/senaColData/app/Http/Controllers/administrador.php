<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\herramienta;
use App\prueba;
use App\plantillasReportes;
use App\requisitosprueba;
use App\Random;
use App\usuario;
use App\Departamento;
use App\ciudad;
use App\CentroFormacion;
use App\herramientaTamp;
use App\Rol;
use App\tipoUsuario;
use App\token;
use Mail;
use Session;

class administrador extends Controller{
  function usuarios(){
  try {
      $res = $_GET['data'];
      $json = json_decode($res);
      switch ($json->estado) {
        case '0'://consultar usuarios
            $resultSet =  DB::select("Select * from userTable");
            return response()->json($resultSet);
          break;
        case'1'://consultar departamentos
            $resultSet = Departamento::select("idDepartamento","Nombre_Departamento")
                                       ->get();
            return response()->json($resultSet);
          break;
        case'2'://consultar ciudades del departamento x
            $resultSet = ciudad::select("idCiudad","nombreCiudad")
                        ->where("fkDepartamento",$json->departamento)
                        ->get();
            return response()->json($resultSet);
          break;
        case'3'://consultar centro de formacion de la ciudad x
            $resultSet = CentroFormacion::select("idCentroFormacion","nombreCentroFormacion")
                        ->where("fkCiudad",$json->ciudad)
                        ->get();
            return response()->json($resultSet);
          break;
        case '4'://consutar roles disponibles
            $resultSet = Rol::select("idRol","nombreRol")
                        ->where("tipoRol",0)
                        ->get();
            return response()->json($resultSet);
          break;
        case'5'://consultar pruebas existentes
            $resultSet = Rol::select("idRol","nombreRol")
                        ->where("tipoRol",1)
                        ->get();
            return response()->json($resultSet);
          break;
        case'6'://Agregar usuario
            $newUser = new usuario;
            $newUser->documento=$json->documento;
            $newUser->nombres=$json->nombre;
            $newUser->apellidos=$json->apellido;
            $newUser->correoElectronico=$json->correo;
            $newUser->password="";
            $newUser->estado=0;
            $newUser->fkCentroFormacion=$json->centroFormacion;
            if ($newUser->save()) {
              $tRol = explode(",",$json->TUsuario);
              $countUsuario=0;
              $countItems=-1;
              $items=[];

                $tipoUsuario = new tipoUsuario;
                $tipoUsuario->fkRol=$json->TUsuario;
                $tipoUsuario->fkUsuario=$newUser->idUsuario;
                if ($tipoUsuario->save()) {
                  if ($json->TUsuario==3){
                    $countItems=0;
                    $items = explode(",",$json->arrPrueba);
                    for ($i=0; $i <count($items) ; $i++) {
                      $pruebaHabilitada = new tipoUsuario;
                      $pruebaHabilitada->fkRol=$items[$i];
                      $pruebaHabilitada->fkUsuario=$newUser->idUsuario;
                      if ($pruebaHabilitada->save()) {
                        $countItems++;
                      }
                    }
                  }
                  $countUsuario++;
                }
              $neg=-1;
              if ((($countUsuario>0) &&  ($countItems== $neg))||((count($items)==($countItems)))) {
                $dates=date('y/m/d', time());
                $tokenHash = password_hash($newUser->documento, PASSWORD_BCRYPT);
                $token = new token;
                $token->numeroToken=$tokenHash;
                $token->fecha=$dates;
                $token->documento=$newUser->documento;
                $user=$json->correo;
                if ($token->save()) {
                  $correoFin = (string)$json->correo;
                  $user=$correoFin;
                  $data=["datSolicitud"=> "http://localhost:8080/proyectos/Dinamico/senaColData/public/cambioContrasena?op=".$token->numeroToken."&usu=".$json->documento,"documento"=>$json->documento,"nombre"=>$json->nombre.' '.$json->apellido];
                  Mail::send('Plantilla.correoEnvio', $data, function ($message) use ($user){
                      $message->subject('senaColData. Cambiar contraseña');
                      $message->to($user);
                  });
                  return ("true$%Se a creado el usuario ".$json->nombre." ".$json->apellido);
                }
              }else{
                return ("false$%No se a creado el usuario ".$json->nombre." ".$json->apellido);
              }
            }else{
              return ("false$%No se a creado el usuario ".$json->nombre." ".$json->apellido);
            }
          break;
        case '7': //modificar un usuario
            $user = usuario::find($json->usuario);
            $user->nombres=$json->nombre;
            $user->apellidos=$json->apellido;
            $user->correoElectronico=$json->correo;
            $user->fkCentroFormacion=$json->centroFormacion;
            if ($user->save()) {
              $items=[];
                $countItems=-1;
                  if ($json->TUsuario==3){
                    $countItems=0;
                    $itemsEliminacion = explode(",",$json->arrPrueba);
                    for ($jq=0; $jq <count($itemsEliminacion) ; $jq++) {
                        if (DB::select("Delete from TipoUsuario where fkUsuario='".$user->idUsuario."' and  fkRol='".$itemsEliminacion[$jq]."")) {
                          $countItems++;
                        }
                    }
                    $items = explode(",",$json->arrPrueba);
                    for ($i=0; $i <count($items) ; $i++) {
                      $pruebaHabilitada = new tipoUsuario;
                      $pruebaHabilitada->fkRol=$items[$i];
                      $pruebaHabilitada->fkUsuario=$newUser->idUsuario;
                      if ($pruebaHabilitada->save()) {
                        $countItems++;
                      }
                    }
                  }
                  $neg=-1;
              if ( (( ($TpTesting <> 3)&&($countItems==$neg) )  || (((count($items)+(count($itemsEliminacion))) ==($countItems)))) ) {
                  return ("true$%Se a modificado el usuario ".$user->nombres." ".$newUser->apellidos);
                }else{
                  return ("false$%No se a modificado el usuario ".$user->nombres." ".$user->apellidos);
                }
            }else{
              return ("false$%No se a modificado el usuario ".$user->nombres." ".$user->apellidos);
            }
          break;
        case '8'://Deshabilitar un usuario
            $user = usuario::find($json->user);
            $user->estado=2;
            if ($user->save()) {
              return ("true$%Se a deshabilitado el usuario ".$user->nombres." ".$user->apellidos);
            }else{
              return ("false$%No se a deshabilitado el  usuario ".$user->nombres." ".$user->apellidos);
            }
          break;
        case '9'://obtener roles del usuario especificado
            $resultSet =  DB::select("call consultaModificarUsuario(".$json->user.")");
            return response()->json($resultSet);
          break;
      }
  } catch (Exception $e) {
    return  $e->getMessage();
  }
}

function pruebas(){
  try {
    if (isset($_GET['data'])) {
      $res = $_GET['data'];
    }else {
      $res = $_POST['data'];
    }
    $json = json_decode($res);
    switch ($json->estado) {
      case '0'://consultar
          $resultSet = prueba::Select('idPrueba','nombre','descripcion','estado')
                    ->get();
          return response()->json($resultSet);
        break;
      case '1'://Agregar
          $aPrueba = new prueba;
          $aPrueba->nombre =$json->nombre;
          $aPrueba->descripcion =$json->descripcion;
          $aPrueba->estado=0;
          $itemsCount=0;
          $arrItems = explode("$$",$json->Items);
          if ($aPrueba->save()) {
            for ($o=0; $o < count($arrItems) ; $o++) {
              if ($o>0) {
                $items = explode(",",$arrItems[$o]);
                $item = new requisitosprueba;
                $item->fkPrueba= $aPrueba->idPrueba;
                $item->nombre=$items[0];
                $item->descripcion=$items[1];
                  if ($item->save()) {
                    $itemsCount++;
                  }
              }
            }
            $rolPrueba = new Rol;
            $rolPrueba->nombreRol=$json->nombre;
            $rolPrueba->descripcion= "Autorizacion para hacer pruebas de ".$json->nombre;
            $rolPrueba->tipoRol=1;
            $rolPrueba->fkPrueba=$aPrueba->idPrueba;
            if ($rolPrueba->save()) {
              $itemsCount++;
            }
            if (($itemsCount)==count($arrItems)) {
              return ("true$%Se a ingresado la prueba ".$json->nombre);
            }else{
              return ("false$%No se a ingresado la prueba ".$json->nombre);
            }
          }else{
            return ("false$%No se a ingresado la prueba ".$json->nombre);
          }
        break;
      case '2'://Modificar
          $aPrueba = prueba::find($json->prueba);
          $aPrueba->nombre  =$json->nombre;
          $aPrueba->descripcion =$json->descripcion;
          $aPrueba->estado=$json->estadoModificado;
          if ($aPrueba->save()) {
            $resultSet= ("true$%Se a modificado la prueba ".$json->nombre);
          }else{
            $resultSet= ("false$%No se a modificado la prueba ".$json->nombre);
          }
          return $resultSet;
        break;
      case '3'://Deshabilitar prueba seleccionada
          $aPrueba = prueba::find($json->prueba);
          $aPrueba->estado =1;
          if ($aPrueba->save()) {
            $resultSet= ("true$%Se a deshabilitado la prueba ".$aPrueba->nombre);
          }else{
            $resultSet= ("false$%No se a deshabilitado la prueba ".$aPrueba->nombre);
          }
          return $resultSet;
        break;
      case '4'://select herramientas prueba
          $resultSet = herramienta::Select('idPrueba','nombre','descripcion')
                ->get();
          return response()->json($resultSet);
          break;
      case '5'://select items prueba
        $resultSet = requisitosprueba::Select('idItem','nombre','descripcion')
                  ->where('fkPrueba',$json->prueba)
                  ->get();
        return response()->json($resultSet);
        break;
      case '7'://consultar una plantilla
          $resultSet = plantillasReportes::Select('idPlantilla','nombrePlantilla','fechaVigencia','formatoPlantilla','estado')
                ->where('fkPrueba2',$json->prueba)
                ->get();
          return response()->json($resultSet);
        break;
      case '8'://insertar una plantilla
          $arrUrl=[];
          $dates=date('y/m/d', time());
          if (isset($_FILES['archivo'])) {
            $dateF=str_replace('/','_',$dates);
            foreach ($_FILES['archivo']['name'] as $indice => $archivo) {
              if (!empty($archivo)) {
                $tipoDato =new Random();
                $nomAleatoria = $tipoDato->randomArchivos(10);
                $nomArchivoD = $nomAleatoria.$dateF.Session::get('idUser');
                move_uploaded_file(
                  $_FILES['archivo']['tmp_name'][$indice],
                  'Archivos/defecto/'.$archivo
                );
                $ext = pathinfo($archivo, PATHINFO_EXTENSION);
                rename("Archivos/defecto/".$archivo, "Archivos/pruebas/plantillas/".$nomArchivoD.'.'.$ext);
                $nomArchivoF=$nomArchivoD.'.'.$ext;
                array_push($arrUrl,$nomArchivoF);
              }
            }
          }
          $nPlantilla = new plantillasReportes;
          $nPlantilla->nombrePlantilla=$_POST['nombrePlantilla'];
          $nPlantilla->formatoPlantilla=$arrUrl[0];
          $nPlantilla->fechaVigencia=$_POST['fechaVigencia'];
          $nPlantilla->estado=0;
          $nPlantilla->fkPrueba2=$_POST['prueba'];
          if ($nPlantilla->save()) {
            $resultSet= ("true$%Se a ingresado el item ".$nPlantilla->nombrePlantilla);
          }else{
            $resultSet= ("false$%No se a ingresado el item ".$nPlantilla->nombrePlantilla);
          }
          return$resultSet;
        break;
      case '9'://deshabiliatar una plantilla
          $aPrueba = plantillasReportes::find($json->plantilla);
          $aPrueba->fechaVigencia =date('y-m-d', time());
          $aPrueba->estado =1;
          if ($aPrueba->save()) {
            return  ("true$%Se a modificado la prueba ".$aPrueba->nombrePlantilla);
          }else{
            return  ("false$%No se a modificado la prueba ".$aPrueba->nombrePlantilla);
          }

        break;
      case '10'://agregar  herramientas
          $ar= explode(',', $json->Items);
          $are= explode(',', $json->arrE);
          for ($r=0; $r < count($are); $r++) {
            $item = herramientaTamp::find($are[$r]);
            if ($item !=null) {
              $item->delete();
            }
          }
          $cont=0;
          for ($q=0; $q < count($ar) ; $q++) {
            if ($ar[$q]!=null) {
              $herramienta = new herramientaTamp;
              $herramienta->idPrueba= $json->pruebaSelect;
              $herramienta->idHerramienta= $ar[$q];
              if ($herramienta->save()) {
                $cont++;
              }
            }
          }
          if ($cont == (count($ar)) ) {
            return  ("true$%Se a modificado la prueba ");
          }else{
            return  ("true$%Se a modificado la prueba ");
          }
        break;
      case '11'://consultar herramientas de la prueba
            $resultSet =  DB::select("call herramientaPrueba(".$json->prueba.")");
            return response()->json($resultSet);
          break;
      case '12':
          $ar="";
          if ($json->items !=false) {
            $ar=$json->items;
            $resultSet = DB::select("select idHerramienta as herramienta,nombreHerramienta as nombre from herramienta where idHerramienta not in (".$ar.")");
            return response()->json($resultSet);
          }else {
            $resultSet= DB::select("select idHerramienta as herramienta,nombreHerramienta as nombre from herramienta");
            return response()->json($resultSet);
          }
        break;
    }
  } catch (Exception $e) {
    echo $e->getMessage();
    return  $e->getMessage();
  }
}

function Herramienta(){
  try {
    $res =$_GET['data'];
    $json = json_decode($res);
    switch ($json->estado) {
      case '0'://consultar
          $resultSet = herramienta::Select('idHerramienta','nombreHerramienta','descripcionHerramienta','urlHerramienta')
                    ->get();
          return response()->json($resultSet);
        break;
      case '1'://Agregar
          $aHerramienta = new herramienta;
          $aHerramienta->nombreHerramienta =$json->nombre;
          $aHerramienta->descripcionHerramienta =$json->descripcion;
          $aHerramienta->urlHerramienta =$json->url;
          if ($aHerramienta->save()) {
            $resultSet= ("true$%Se a ingresado la herramienta ".$json->nombre);
          }else{
            $resultSet= ("false$%No se a ingresado la herramienta ".$json->nombre);
          }
          return $resultSet;
        break;
      case '2'://Modificar
          $aHerramienta =  herramienta::find($json->herramienta);
          $aHerramienta->nombreHerramienta =$json->nombre;
          $aHerramienta->descripcionHerramienta =$json->descripcion;
          $aHerramienta->urlHerramienta =$json->url;
          if ($aHerramienta->save()) {
            $resultSet= ("true$%Se a modificado la herramienta " .$json->nombre);
          }else{
            $resultSet= ("false$%No se a modificado la herramienta ").$json->nombre;
          }
          return $resultSet;
        break;
      case '3'://Eliminar
          $resultSet = DB::select("delete from Heramientamp where idHerramienta=".$json->herramienta."");
          $aHerramienta =  herramienta::find($json->herramienta);
          $nom =$aHerramienta->nombreHerramienta;
          if ($aHerramienta->delete()) {
            $resultSet= ("true$%Se a eliminado la herramienta".$nom);
          }else{
            $resultSet= ("false$%No se a eliminado la herramienta".$nom);
          }
          return $resultSet;
        break;
    }
  } catch (Exception $e) {
    echo $e->getMessage();
    return  $e->getMessage();
  }
}
}
