<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;

class linksSolicitante extends Controller
{
  function BuscarModulo(){
  try {
    $res =$_GET['data'];
    $json = json_decode($res);
    if ($json->tip==11) {
      switch ($json->option) {
        case 0:
            return view('solicitante/Solicitud');
          break;
        case 1:
            return view('solicitante/ConsultarSolicitud');
          break;
        case 2:
            $this->cerraT();
            return view('incio/index');
          break;
        default:
            $this->cerraT();
            return view('incio/index');
          break;
        }
    }else{
          $this->cerraT();
          return view('incio/index');
    }
  } catch (Exception $e) {
    $this->cerraT();
    return view('incio/index');
  }
 }
 function cerraT(){
   Session::forget('idTUsuario');
   Session::forget('idUser');
   Session::forget('idCentro');
   return view('incio/index');
  }

}
