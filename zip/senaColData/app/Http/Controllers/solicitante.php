<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\prueba;
use App\solicitud;
use App\Documentosolicitud;
use App\itemsprueba;
use App\EvaluacionSolicitud;
use Hash;
use Session;
use PDF;
use App\Random;

class solicitante extends Controller{
  function ModificarSolicitudes(){
    try {
      $conArchivos =0;
      $arrUrl=[];
      $arrayArchivo="";
      $arrayArchivo = explode(",", $_POST['arrArchivos']);
      $dates=date('y/m/d', time());
      if (isset($_FILES['archivo'])) {
        $dateF=str_replace('/','_',$dates);
        foreach ($_FILES['archivo']['name'] as $indice => $archivo) {
          if (!empty($archivo)) {
            $tipoDato =new Random();
            $nomAleatoria = $tipoDato->randomArchivos(10);
            $nomArchivoD = $nomAleatoria.$dateF.Session::get('idUser');
            move_uploaded_file(
              $_FILES['archivo']['tmp_name'][$indice],
              'Archivos/defecto/'.$archivo
            );
            $ext = pathinfo($archivo, PATHINFO_EXTENSION);
            rename("Archivos/defecto/".$archivo, "Archivos/solicitudes/documentosSolicitud/".$nomArchivoD.'.'.$ext);
            $nomArchivoF=$nomArchivoD.'.'.$ext;
            array_push($arrUrl,$nomArchivoF);
          }
        }
      }
      $soli = solicitud::find($_POST['solicitud']);
      if ($soli->fkEstado==6) {
        $soli->fkEstado =1;
      }else if($soli->fkEstado==8){
        $soli->fkEstado=3;
      }
      $soli->fechaFinalizacion = null;
      if ($soli->save()) {
        $conArchivos++;
      }
      for ($i=0; $i < count($arrayArchivo); $i++) {
        $infoArchivo = explode("$$",$arrayArchivo[$i]);
         unlink('Archivos/solicitudes/documentosSolicitud/'.$infoArchivo[0]);
        try {
        } catch (Exception $e) {

        }
        $itemBD =  Documentosolicitud::find($infoArchivo[1]);
        $itemBD->urlArchivo= $arrUrl[$i];
        $itemBD->estadoDocumento=0;
        if ($itemBD->save()) {
          $conArchivos++;
        }
      }
      if (($conArchivos)==(count($arrayArchivo)+1)) {
        $resultSet="true$%Se a modificado la solicitud";
      }else{
        $resultSet= ("false$%No se a modificado la solicitud");
      }
      return $resultSet;
    } catch (Exception $e) {
      return  $e->getMessage();
    }
  }

  function pruebasAll(){
    try {
      $resultSet = Prueba::Select('idPrueba','nombre')
                ->where('estado',0)
                ->get();
      return response()->json($resultSet);
    } catch (Exception $e) {
      echo $e->getMessage();
      return  $e->getMessage();
    }
 }

function Itemsprueba(){
  try {
    $res =$_GET['data'];
    $json = json_decode($res);
    $resultSet = DB::select("call conItemsRequisitos(".$json->option.")");

  } catch (Exception $e) {
    echo $e->getMessage();
    $resultSet= $e->getMessage();
  }
  return response()->json($resultSet);
}

function crearSolicitud(){
    try {
      $date=date('y/m/d h:i:s', time());
      $dates=date('Y/m/d', time());
      $tipoDato =new Random();
      $token= $_POST['pruebaSolicitud'].Session::get('idUser').$tipoDato->randomToken(4).date('m', time()).date('d', time()).date('his', time());
      $solicitud = new solicitud;
      $solicitud->nombreSolicitud= $_POST['nombreSolicitud'];
      $solicitud->descripcionSolicitud= $_POST['DescripcionSolicitud'];
      $solicitud->fechaSolicitud  ;
      $solicitud->ticked=$token;
      $solicitud->fksolicitante= Session::get('idUser');
      $solicitud->idPrueba= $_POST['pruebaSolicitud'];
      $solicitud->fkEstado=1;
      $solicitud->fkCentro= Session::get('idCentro');
      $cc=0;
      $arrArchivos = explode(",", $_POST['ItemspruebasLabel']);
      if ($solicitud->save()) {
       $arrUrl=[];
         if (isset($_FILES['archivo'])) {
           $dateF=str_replace('/','_',$dates);
           foreach ($_FILES['archivo']['name'] as $indice => $archivo) {
             if (!empty($archivo)) {
               $tipoDato =new Random();
               $nomAleatoria = $tipoDato->randomArchivos(8);
               $nomArchivoD = $nomAleatoria.$dateF.Session::get('idUser');
               move_uploaded_file(
                 $_FILES['archivo']['tmp_name'][$indice],
                 'Archivos/defecto/'.$archivo
               );
               $ext = pathinfo($archivo, PATHINFO_EXTENSION);
               rename("Archivos/defecto/".$archivo, "Archivos/solicitudes/documentosSolicitud/".$nomArchivoD.'.'.$ext);
               $nomArchivoF=$nomArchivoD.'.'.$ext;
               array_push($arrUrl,$nomArchivoF);
             }
           }
         }
         for ($i=0; $i <count($arrArchivos) ; $i++) {
            $item = new Documentosolicitud;
             if ($arrArchivos[$i]=="Elemento$%s") {
               $item->fktipoDocument=4;
             }else{
               $item->fktipoDocument=1;
             }
            $item->fkSolicitud=$solicitud->idSolicitud;
            $item->fechaDocumento=$date;
            $item->descripcionDocumento=$arrArchivos[$i];
            $item->urlArchivo=$arrUrl[$i];
            $item->estadoDocumento=0;
            if ($item->save()) {
              $cc++;
            }else{
              $cc=$cc-1;
            }
          }
          $s= count($arrArchivos);
          if ($s == $cc) {
              $resultSet="true$%Se a creado la solicitud";
          }else{
              $resultSet= ("false$%No se a creado la solicitud");
          }
      }else{
          $resultSet= ("false$%No se a ingresado la solicitud");
      }
    } catch (Exception $e) {
        echo $e->getMessage();
        $resultSet=$e->getMessage();
    }
   return $resultSet;
}

function ConsularSolicitud(){

  try {
    $resultSet = DB::select("
            select t1.idSolicitud,t1.nombreSolicitud,t1.fechaSolicitud,t1.fechaFinalizacion,t1.ticked,t2.idEstado,t2.descripcionEstado as nombreEstado
            from Solicitud  t1
            inner join Estado t2 on t1.fkEstado=t2.idEstado
            where fksolicitante= '".Session::get('idUser')."'") ;
  } catch (Exception $e) {
    echo $e->getMessage();
    $resultSet=$e->getMessage();
  }
   return $resultSet;
}
function detalleSolicitud(){
  try {
    $res =$_GET['data'];
    $json = json_decode($res);
    switch ($json->estado) {
      case '0': //detallesbase
          $resultSet = DB::select("call detallesConsultaSoliB(".Session::get('idUser').",".$json->solicitud.")");
          return $resultSet;
        break;
      case '1':  //informes previos y finales
        $resultSet = DB::select("call informesT(".$json->solicitud.")");
        return $resultSet;
        break;
      case '2': //informe de resultados
          if ($json->solicitudEstado==6) {//carga los documentos requeridos de la prueba
            $resultSet=Documentosolicitud::select('idCertificadoSolicitud','fkSolicitud','urlArchivo','descripcionDocumento')
            ->where('fkSolicitud',$json->solicitud)
            ->where('fktipoDocument',1)
            ->get();
             return $resultSet;
          }else{
            $datosSolicitud = DB::select("call solicitudPdf(".$json->solicitud.")");
            $arrItems = EvaluacionSolicitud::Select('Evaluacionsolicitud.valoracion','Evaluacionsolicitud.observacion','RequisitosPrueba.nombre')
                ->join('RequisitosPrueba','RequisitosPrueba.idItem','=','Evaluacionsolicitud.fkItemPrueba')
                ->where('Evaluacionsolicitud.fkSolicitud',$json->solicitud)
                ->get();
             $pdf = PDF::loadView('Plantilla/plantilla',["datSolicitud"=> $datosSolicitud,"items"=>$arrItems]);
             return $pdf->download('informe_Resultados.pdf');
          }
        break;
      case '3':
        $resultSet = Documentosolicitud::select('idCertificadoSolicitud','fkSolicitud','urlArchivo','descripcionDocumento')
        ->where('fkSolicitud',$json->solicitud)
        ->where('fktipoDocument',1)
        ->where('estadoDocumento',1)
        ->get();
       return $resultSet;
      break;
    }
  } catch (Exception $e) {
    echo $e->getMessage();
    $resultSet=$e->getMessage();
    return $resultSet;
  }
}

}
