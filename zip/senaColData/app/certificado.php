<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class certificado extends Model
{
  protected $table="Certificado";
  protected $primaryKey="idCertificado";
  public $timestamps=false;
}
