<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nombre extends Model
{
  protected $table="Nombre";
  protected $primaryKey="idNombre";
  public $timestamps=false;
}
