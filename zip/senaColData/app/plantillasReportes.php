<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class plantillasReportes extends Model
{
  protected $table="plantillasReportes";
  protected $primaryKey="idPlantilla";
  public $timestamps=false;
}
