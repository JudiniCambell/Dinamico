<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terriorio extends Model
{
  protected $table="Territorio";
    protected $primaryKey="idTerritorio";
    public $timestamps=false;
}
