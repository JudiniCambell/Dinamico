$(document).ready(function(){
  var selector = [], hilo = [], jso = [], data = [],datos=[];
  var ob = new $.Luna("Solicitud");
  ob.Vivo("Solicitud1");
  urlrelativo = ob.Relativo();
  selector[0]= $("#pruebaSolicitud");
  datos[0] = {nombre:"Select"};
  jso[0] = {
    url: '../../pruebasAl',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: "null"
  };
  ajax(0);

  $("#form").submit(function(){
    $.ajax({
      url: urlrelativo+'/crearSolicitud',
      type:'post',
      data: new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      success:function(resultSet){
        resDocument(resultSet);
      },
      error:function(){
        alert("Error fatal comuniquese con el administrador del sistema");
      }
    });
    return false;
  });

  $("#pruebaSolicitud").change(function(){
    $("#Itemsprueba option[value='A0']").attr("selected",true);
    var s = "<label id='count' name='count'  style='display: none;'></label>"
    $("#resultItems").html("").append(s);
    if($("#pruebaSolicitud").val()!="A0"){
      selector[1]= $("#Itemsprueba");
      datos[1] = {nombre:"Itemsprueba"};
      datJson ='{"option":"'+$("#pruebaSolicitud").val()+'"}';
      jso[1] = {
        url: '../../Itemsprueba',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(1);
    }
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          hilo[i].terminate();
          peticionCompleta(i);
      };
  };


  function resDocument(resultSet){
    var daMen = resultSet.split("$%");
    var men = "";
    if (daMen[0] == "true") {
        estado = ("success");
        men = daMen[1]+" "+$("#nombreSolicitud").val();
    } else {
        estado = ("error");
        men = daMen[1]+" "+$("#nombreSolicitud").val();
    }
    $("#btnAgregar").prop('disabled', false);
    $.notify(men, estado);

    datos[2]={opcion:1,icon:"eye",label:"#stroked-eye",titulo:"Consultar solicitudes"};
    datJson='{"option":"'+1+'","tip":"11"}';
    jso[2] = {
      url: '../../linkSolicitante',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(2);
  }

  function peticionCompleta(i){
    if (i==2) {
      $("#contenido").html("");
      $("#contenido").html(data[i]);
      $("#useClassModulo").attr('class','');
      $("#useClassModulo").attr('class','glyph stroked '+datos[2].icon+'');
      $("#useClass1Modulo").attr('xlink:href',datos[2].label);
      $("#titulo").text(datos[0].titulo);
    }
  }
});
