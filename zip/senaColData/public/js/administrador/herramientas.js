$(document).ready(function(){
  $("#acordion").accordion({heightStyle: 'content'});
  var selector = [], hilo = [], jso = [], data = [],datos=[],herramienta=0;
  var ob = new $.Luna("herramienta");
  ob.Vivo("herramienta");
  selector[0]= $("#tableHerramientas");
  ob.TablaEspa(selector[0]);
  datos[0] = {nombre:"HerramientasConsulta"};
  datJson = '{"estado":"0"}';
  jso[0] = {
    url:'../../herramienta',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: datJson
  };
  ajax(0);

  $(document).on('click','.btnModificar',function(e){
    var arr =  this.id.split("$$");
    herramienta=arr[0];
    $("#nombreHerramienta").val(arr[1]);
    $("#descripcionHerramienta").val(arr[2]);
    $("#urlHerramienta").val(arr[3]);
    $('html, section').animate({scrollTop:0}, 'slow');
    $("#btnAccion").text("Modificar");
  });

  $(document).on('click','.btnEliminarHerramienta',function(e){
    datos[1] = {nombre:"btn"};
    datJson = '{"herramienta":"'+this.id+'","estado":"3"}';
    jso[1] = {
      url: Base +'herramienta',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });
  $("#btnAccion").click(function(){
    if ($("#btnAccion").text()=="Agregar herramienta") {
      estado="1"; //Agregar herramienta
    }else{
      estado="2";//Modificar herramienta
    }
    datJson = '{"herramienta":"'+herramienta+'","nombre":"'+$("#nombreHerramienta").val()+'","descripcion":"'+$("#descripcionHerramienta").val()+'","url":"'+$("#urlHerramienta").val()+'","estado":"'+estado+'"}';
    jso[1] = {
      url:'../../herramienta',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });
  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          ajaxResult(i);
          hilo[i].terminate();
      };
  };
var estado =0,reporteValoracion="";
  function ajaxResult(i){
    if(i==1){
      var daMen = data[i].split("$%");
      var men = "";
      if (daMen[0] == "true") {
          estado = ("success");
          men = daMen[1];
      } else {
          estado = ("error");
          men = daMen[1];
      }
      $.notify(men, estado);
      selector[0]= $("#tableHerramientas");
      datos[0] = {nombre:"HerramientasConsulta"};
      datJson = '{"estado":"0"}';
      jso[0] = {
        url:'../../herramienta',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      $("#nombreHerramienta").val("");
      $("#urlHerramienta").val("");
      $("#descripcionHerramienta").val("");
      ajax(0);
    }
  }
});
