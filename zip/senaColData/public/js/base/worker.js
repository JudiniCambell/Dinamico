this.addEventListener('message', function(e) {
  var data = e.data;
  var xhr = new XMLHttpRequest();
  xhr.open("POST", data.url + "?data=" + data.dat, true);
  xhr.setRequestHeader('X-CSRF-TOKEN', data.token);
  xhr.onload = function(e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        respuesta(xhr.responseText)
      } else {
        console.error(xhr.statusText);
      }
    }
  };
  xhr.onerror = function(e) {
    console.log(e);
    console.error(xhr.statusText);
  };
  xhr.send(data.dat);

  function respuesta(obj) {
    this.postMessage(obj);
    obj = null;
  }
}, false);
