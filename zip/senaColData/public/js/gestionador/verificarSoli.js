$(document).ready(function(){
  var selector = [], hilo = [], jso = [], data = [],datos=[],solicitud=0;
  var ob = new $.Luna("Solicitudes",null);
  ob.Vivo("Solicitudes");
  selector[0]= $("#tablaVerificarLista");
  datJson = '{"centro":"'+$("#centro").text()+'"}';
  datos[0] = {nombre:"verificarSolicitud","btn":"Verificar"};
  ob.TablaEspa(selector[0]);
  jso[0] = {
    url: '../../ConSolicitudes',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: "null"
  };
  ajax(0);
  $("#selectTestingP").change(function(){
    $("#pruebas").empty();
    if ($("#selectTestingP").val()!="A0") {
      selector[4]= $("#pruebas");
      datos[4] = {nombre:"Select"};
      datJson = '{"option":"'+$("#selectTestingP").val()+'"}';
      jso[4] = {
        url: '../../solicitudesTesting',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(4);
    }
  });
  var tiJson =3;
  $(document).on('click','.radioItem',function(e){
    var co=0,ind=0;
    $(".radioItem").each(function (index) {
     if($(this).is(':checked')){
       if ($(this).attr("id") =="siOpcionItem") {
         co++;
       }
     }
     ind++;
    });
    ind=ind/2;
    if ((co+1)==ind) {
      $("#SolicitudFalla").hide();
      $("#SolicitudBuena").show();
      tiJson=1;
    }else{
      $("#SolicitudFalla").show();
      $("#SolicitudBuena").hide();
      tiJson=0;
    }
    $("#divBtn").show();
  });

  $(document).on('click','.btnVerificarSG',function(e){
    $("#EspacioTable").hide();
    var dselect= this.id;
    var dat= dselect.split("$$");
    $("#NombreP").text(dat[2]);
    $("#DescripcionP").text(dat[3]);
    $("#RequePrueba").text("Documentos requeridos de la prueba "+dat[4]);
    $("#formVerifica").show();
    selector[1]= $("#RequerimientoSolicitud");
    solicitud=dat[0];
    datos[1] = {nombre:"Select","solicitud":dat[0],"PruebaVer":dat[1],opt:"limpiaM"};
    datJson ='{"option":"'+dat[1]+'"}';
    jso[1] = {
      url:'../../RequerimientoPrueba',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  $("#btnEnviar").click(function(){
    datos[5] = {nombre:"btn"};
    if (tiJson==1) {
      datJson = '{"solicitud":"'+solicitud+'","tiJson":"'+tiJson+'","testingSelec":"'+$("#selectTestingP").val()+'"}';
    }else if (tiJson==0){
      var arrItems =[];
      $(".radioItem").each(function (index) {
       if($(this).is(':checked')){
         if ($(this).attr("id") =="noOpcionItem") {
           arrItems.push($(this).attr("value"));
         }
       }
      });
      datJson = '{"solicitud":"'+solicitud+'","tiJson":"'+tiJson+'","casoError":"'+$("#textAreaFlaso").val()+'","itemsFalla":"'+arrItems+'"}';
    }
    jso[5] = {
      url: '../../EnviarSolicitud',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(5);
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          hilo[i].terminate();
          peticionCompleta(i);
      };
  };
  function peticionCompleta(i) {
    if(i ==1){
      selector[2]= $("#itemsDocumetosSolicitud");
      datos[2] = {nombre:"RequerimientoSolicitud"};
      datJson ='{"prueba":"'+datos[1].solicitud+'"}';
      jso[2] = {
        url: '../../DocumentosSolicitud',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
    ajax(2);
    }else if(i==2){
      selector[3]= $("#selectTestingP");
      datos[3] = {nombre:"Select",opt:"limpiaN"};
      datJson = '{"prueba":"'+datos[1].PruebaVer+'"}';
      jso[3] = {
        url: '../../testingPrueba',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(3);
    }else if (i==4){
      if ($("#pruebas").html()=="") {
        $("#VSolicitud").text("");
        var option = "<option value='A0'>No tiene ninguna solicitud asignada</option>";
        $("#pruebas").append(option);
      }else{
        $("#VSolicitud").text("Solicitudes asignadas");
      }
    }else if(i==5){
      var daMen = data[i].split("$%");
      var men = "";
      if (daMen[0] == "true") {
          estado = ("success");
          men = daMen[1];
      } else {
          estado = ("error");
          men = daMen[1];
      }
      $("#btnAgregar").prop('disabled', false);
      $.notify(men, estado);
      datos[6]={opcion:0,icon:"checkmark",label:"#stroked-checkmark",titulo:"Verificar solicitud"};
      datJson='{"option":"'+0+'","tip":"12"}';
      jso[6] = {
        url:'../../linkGestor',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(6);
    }else if(i==6){
      $("#contenido").html("");
      $("#contenido").html(data[i]);
      $("#useClassModulo").attr('class','');
      $("#useClassModulo").attr('class','glyph stroked '+datos[0].icon+'');
      $("#useClass1Modulo").attr('xlink:href',datos[0].label);
      $("#titulo").text(datos[0].titulo);
    }
  }
});
