$(document).ready(function(){
  var obsession = {"tipoUsuario":"13"};
  var selector = [], hilo = [], jso = [], data = [],datos=[],datJson="";
  var ob = new $.Luna("principalExperto");
  ob.Vivo("principalExperto");

  datos[0]={opcion:'0',icon:"pencil",label:"#stroked-pencil",titulo:"Verificar solicitud"};
  datJson='{"option":"'+0+'","tip":"'+obsession.tipoUsuario+'"}';
  jso[1] = {
    url: '../../linkExperto',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: datJson
  };
  ajax(1);
  $(document).on('click','.labelS',function(e){
    var s = this.id;
    $("#btnIngresar").prop('disabled', true);
    switch (s) {
      case '0':
        datos[0]={opcion:s,icon:"checkmark",label:"#stroked-checkmark",titulo:"Verificar solicitud"};
        break;
      case '1':
        datos[0]={opcion:s,icon:"clipboard with paper",label:"#stroked-clipboard-with-paper",titulo:"Realizar pruebas"};
        break;
      case '2':
        datos[0]={opcion:s};
          break;
    }
    datJson='{"option":"'+s+'","tip":"'+obsession.tipoUsuario+'"}';
    jso[1] = {
      url: '../../linkExperto',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          hilo[i].terminate();
          peticionCompleta(i);
      };
  };

  function peticionCompleta(i) {
      if(i==1){
        if (datos[0].opcion=='2') {
          $("#formComplet").html("");
          $("#formComplet").html(data[i]);
        }else{
          $("#contenido").html("");
          $("#contenido").html(data[i]);
          $("#useClassModulo").attr('class','');
          $("#useClassModulo").attr('class','glyph stroked '+datos[0].icon+'');
          $("#useClass1Modulo").attr('xlink:href',datos[0].label);
          $("#titulo").text(datos[0].titulo);
        }
      }
    }
});
