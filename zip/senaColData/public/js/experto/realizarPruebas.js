$(document).ready(function(){
  var selector = [], hilo = [], jso = [], data = [],datos=[],solicitud=0,arrItems=[],datJson="",arrItems="w",contadorEli=0;
  var ob = new $.Luna("Solicitud");
  ob.Vivo("SolicitudesPruebas");
  urlrelativo= ob.Relativo();
  selector[0]= $("#tablaRealizarP");
  $("#tablaRealizarP  tbody").empty();
  ob.TablaEspa(selector[0]);
  datos[0] = {nombre:"consultarSolcitudesListas"};
  jso[0] = {
    url:'../../consultarSolcitudesListas',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: "null"
  };
  ajax(0);

  $(document).on('click','.radioItem',function(e){
    var co=0,ind=0;
    $(".radioItem").each(function (index) {
     if($(this).is(':checked')){
       if ($(this).attr("id") =="siOpcionItem") {
         co++;
       }
     }
     ind++;
    });
    ind=ind/2;
    if ((co+1)==ind) {
      $("#SolicitudFalla").hide();
      tiJson=1;
    }else{
      $("#SolicitudFalla").show();
      tiJson=0;
    }
    $("#divBtn").show();
  });

  $(document).on('click','.btnDevolver',function(e){
    solicitud=this.id;
    selector[7]= $("#itemsDocumetossk");
    datos[7] = {nombre:"RequerimientoSolicitudExperto"};
    datJson ='{"prueba":"'+solicitud+'"}';
    jso[7] = {
      url: '../../DocumentosSolicitud',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    $("#divTable").hide();
    $("#devolver").show();
    ajax(7);
  });

  $(document).on('click','.btnElimplan',function(e){
    var action= $(this).val();
    var id = action.split("r");
    var busqueda = $.inArray(id[1], arrItems);
    arrItems.splice(busqueda, 1);
    $("#arrayPlantilla").val(arrItems);
    $("#"+action+"").remove();
    contadorEli =contadorEli-1;
    if (contadorEli==0) {
      $("#labelsPlantillas").hide();
    }
  });

  var solicitud=0;


  $(document).on('click','.btnRealizarPrueba',function(e){
    $("#Acordion").accordion({heightStyle: 'content'});
    $('#file-es').fileinput({
      language: 'es'
      //,allowedFileExtensions: ['jpg', 'png', 'gif','csv']
    });
    ob.setCons("Herramientas");
    var arr =  this.id.split("$$");
    solicitud=arr[0];
    $("#NombrePrueba").text(arr[4]);
    $("#DescripcionPrueba").text(arr[5]);
    $("#NombreSolicitud").text(arr[2]);
    $("#DescripcionSolicitud").text(arr[3]);
    $("#divTable").hide();
    $("#divRealizar").show();
    selector[1]= $("#tableHerramientasP");
    ob.TablaEspa(selector[1]);
    $(".solicitud").val(arr[0]);
    datJson = '{"prueba":"'+arr[1]+'"}';
    datos[1] = {nombre:"herramientas",prueba:arr[1]};
    jso[1] = {
      url: '../../herramientas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });


  $("#btnAgregarPlantilla").click(function() {
    if ($("#SelectPlantilla").val() !="A0") {
        datos[4] = {nombre:"btn"};
        datJson = '{"plantilla":"'+$("#SelectPlantilla").val()+'"}';
        jso[4] = {
          url: '../../linkPlantilla',
          token: $('meta[name="csrf-token"]').attr('content'),
          dat: datJson
        };
        ajax(4);
    }
  });

  var arrItemsFail=[];
  $("#btnDevolver").click(function(){
    datos[9] = {nombre:"btn"};
    $(".radioItem").each(function (index) {
     if($(this).is(':checked')){
       if ($(this).attr("id") =="noOpcionItem") {
         arrItemsFail.push($(this).attr("value"));
       }
     }
    });
    datJson = '{"solicitud":"'+solicitud+'","tiJson":"'+tiJson+'","casoError":"'+$("#textAreaFlaso").val()+'","itemsFalla":"'+arrItemsFail+'"}';
    jso[9] = {
      url: '../../VerificarSolicitudExperto',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(9);
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          peticionCompleta(i),
          hilo[i].terminate();
      };
  };

  $("#formresultadoParcial").submit(function(){
    $.ajax({
      url:urlrelativo+'/resultadosparciales',
      type:'POST',
      data: new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      success:function(resultSet){
        resultAjax(resultSet);
      },
      error:function(){
        alert("Error fatal comuniquese con el administrador del sistema");
      }
    });
    return false;
  });


  $("#formResultadoFinal").submit(function(){
    $("input:checkbox:checked").each(function () {
      $(this).val(1);
    });
    $("input:checkbox:not(:checked)").each(function () {
        $(this).val(2);
    });

   $.ajax({
      url:urlrelativo+'/resultadofinal',
      type:'POST',
      data: new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      success:function(resultSet){
        resultAjax(resultSet);
      },
      error:function(){
        alert("Error fatal comuniquese con el administrador del sistema");
      }
    });
    return false;
  });
  var men = "",estado="error";
  function resultAjax(resultSet){
      var daMen = resultSet.split("$%");
      if (daMen[0] == "true") {
          estado = ("success");
          men = daMen[1];
      } else {
          estado = ("error");
          men = daMen[1];
      }
      $("#btnAgregar").prop('disabled', false);
      datos[11]={opcion:1,icon:"clipboard with paper",label:"#stroked-clipboard-with-paper",titulo:"Realizar pruebas"};
      datJson='{"option":"1","tip":"13"}';
      jso[11] = {
        url: '../../linkExperto',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(11);
  }

  var countPlantilla=0
  function peticionCompleta(i){
    if (i==1) {
      selector[2]= $("#itemsDocumetosSolicitud");
      datos[2] = {nombre:"RequerimientoSolicitud1"};
      datJson ='{"prueba":"'+solicitud+'"}';
      jso[2] = {
        url:'../../DocumentosSolicitud',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(2);
    }else if (i==2){
      selector[3]= $("#itemsPruebaClon");
      datos[3] = {nombre:"herramientas"};
      datJson = '{"prueba":"'+datos[1].prueba+'"}';
      datos[3] = {nombre:"ItemsPruebaSelect"};
      jso[3] = {
        url:'../../itemsPruebaExperto',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(3);
    }else if (i==3) {
      $("#SelectPlantilla").empty().append("<option value='A0'>Seleciona...</option>");
      datJson = '{"prueba":"'+datos[1].prueba+'"}';
      selector[12]= $("#SelectPlantilla");
      datos[12] = {nombre:"Select"};
      jso[12] = {
        url:'../../plantillasR',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(12);
    }else if(i==4){
      if (countPlantilla==0) {
        $("#labelsPlantillas").show();
        arrItems=[];
      }

      var jsonL = jQuery.parseJSON(data[i]);
      var clon = $("#aceptarPlantillaClon").clone();
      clon.find(".contenedor").attr('id','contenedor'+countPlantilla);
      clon.find("#nombrePlantilla").text($("#SelectPlantilla option:selected").text());
      clon.find("#desPlantilla").text(jsonL[0].descripcionPlantilla);
      clon.find("#linkDescarga").attr('href','Archivos/pruebas/plantillas/'+jsonL[0].formatoPlantilla);
      clon.find("#btnEliminar").val('contenedor'+countPlantilla);
      clon.children().appendTo($("#formEnvioPlantillas"));
      arrItems.push($("#SelectPlantilla option:selected").text());
      $("#arrayPlantilla").val(arrItems);
      countPlantilla++;
      contadorEli++;
    }else if (i==9) {
      resultAjax(data[i]);
    }else if(i==11){
      $("#contenido").html("");
      $("#contenido").html(data[i]);
      $("#useClassModulo").attr('class','');
      $("#useClassModulo").attr('class','glyph stroked '+datos[0].icon+'');
      $("#useClass1Modulo").attr('xlink:href',datos[0].label);
      $("#titulo").text(datos[0].titulo);
      $.notify(men, estado);
    }
  }
});
