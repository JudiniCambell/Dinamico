<!DOCTYPE html>
<html>
  <head>
    <meta name="csrf-token" content="{{ csrf_token()}}"/>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <label for="">Cargar elementos</label>
    <button type="button" id="btnAgregar">Cargar</button>
  </body>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/notify.js"></script>
  <script type="text/javascript" src="js/cargarArchivo.js"></script>
</html>
