<html>
<head>
  <style>
    header { position: fixed;
       left: 0px;
       top: -160px;
       right: 0px;
       height: 100px;
     }
    table {
      border-collapse: collapse;
      width: 100%;
    }
    th, td {
        text-align: left;
        padding: 8px;
    }
    tr:nth-child(even){background-color: #f2f2f2}

    body{
      font-family: sans-serif;
    }
    @page {
      margin: 160px 50px;
    }
    footer {
      position: fixed;
      left: 0px;
      bottom: -50px;
      right: 0px;
      height: 40px;
      border-bottom: 2px solid #ddd;
    }
    footer .page:after {
      content: counter(page);
    }
    footer table {
      width: 100%;
    }
    footer p {
      text-align: right;
    }
    footer .izq {
      text-align: left;
    }
    .colorazul{
      color: rgb(48, 165, 255);
    }
  </style>
<body>
  <header >
    <table>
      <tr border="1">
        <th><img src="img/logo_sena_negro.png" width="80" height="80"></th>
        <th><h3>Informe resultados de la solicitud</h3></th>
      </tr>
    </table>
  </header>
  <footer>
    <table>
      <tr>
        <td>
          <p class="izq">
              <span class="colorazul">Sena</span>
              <span>Testing Center</span>
            </p>
        </td>
        <td>
          <p class="page">
            Página
          </p>
        </td>
      </tr>
    </table>
  </footer>
  <div id="content">
        <h4>Bogota D.C <?php echo (date('d/m/Y', time())); ?> </h4>
        @foreach ($datSolicitud as $datos)
         <p>
          Se genera el informe a peticion de <b>{{$datos->nombres}}  {{$datos->apellidos}}</b>,
          se a realizado la prueba {{$datos->prueba}}.
        </p>
         <p>
          Realizada entre {{$datos->fechaSolicitud}} y {{$datos->fechaFinalizacion}}
          soportadas por los siguientes criterios de evaluación:
        </p>
        @endforeach
    <p>
      <table>
        <tr>
          <th>N°</th>
          <th>Item</th>
          <th>Resultado</th>
          <th>Observacion</th>
        </tr>
          <?php $cc =1; $valoracion=""; ?>
            @foreach($items as $item)
            <tr>
                <td> <?php echo($cc); $cc++; ?> </td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->valoracion}}</td>
                <td>{{$item->observacion}}</td>
            </tr>
            @endforeach
        </tbody>
      </table>

    </p>
    @foreach ($datSolicitud as $dat)
     <br>
    <br>
     <p>
      La solicitud a sido {{$dat->nombreEstado}} por <b>{{$datos->testingNo}}  {{$datos->testingApe}}</b>.
     </p>
    @endforeach
  </div>
</body>
</html>
