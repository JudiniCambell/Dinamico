<!DOCTYPE html>
<html id="formComplet">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token()}}"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  <title>Sena Testing Center-SenaData</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" ><span>sena</span>testing center</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>{{Session::get('Nombre')}}<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
								<li><a  class="labelS" id="1">Cerrar sesion</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading" id="accordion"><svg id="useClassModulo" class="glyph stroked table"><use xlink:href="#stroked-table" id="useClass1Modulo"></use></svg><label id="titulo">Generador de datos</label></div>
					<div class="panel-body" id="contenido">
            <article  id="contenedorM" class="col-md-11">
                <div class="col-md-6">
                  <label for="selectASalida" class="col-md-12 col-md-offset-2">Archivo de salida </label>
                  <div class="col-md-4 col-md-offset-2">
                    <select  id="selectASalida" class="form-control">
                      <option value="A0">Seleciona...</option>
                      <option value="1">CSV</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="nRegistros"  class="col-md-12">Numero de registros</label>
                  <div class="col-md-4">
                      <input type="number" id="nRegistros" value="0" class="form-control">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-3">
                    <label for="nomColumn" class="col-md-12">Nombre de la columna</label>
                    <div class="col-md-12">
                        <input type="text" id="nomColumn" class="form-control input">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label for="selectTipDato" class="col-md-12">Tipo de dato</label>
                    <div class="col-md-12">
                      <select class="form-control select" id="selectTipDato">
                        <option value="A0">Seleciona...</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-2 div" id="DinamicoFecha" style="display:none;">
                    <br>
                    <label for="CheckIntervalo">¿Con intervalo?</label>
                    <input type="checkbox" id="CheckIntervalo" class="che">
                  </div>
                  <div class="col-md-2 div" id="DinamicoRango" style="display:none;">
                    <br>
                    <label for="Checkrango">¿Con rango?</label>
                    <input type="checkbox" id="Checkrango" class="che">
                  </div>
                  <div class="col-md-3">
                    <label for="taCampo" class="col-md-12">Tamaño del campo</label>
                    <div class="col-md-8">
                        <input type="number" id="taCampo" class="form-control input" value="0">
                    </div>
                  </div>
                  <div class="col-md-1">
                    <br>
                    <button type="button" id="btnAgregarElement"class="btn btn-info ">
                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                  </div>
                  <div class="col-md-10">
                    <br>
                    <div class="div" id="intervaloN" style="display:none;">
                      <label for="CheckIntervalo">Tipo de intervalo</label>
                      <select class="select" id="selectintervalo">
                        <option value="A0">Seleciona...</option>
                        <option value="0">Numerico</option>
                        <option value="1">Alfabetico</option>
                      </select>
                      <label for="intervaloX">Conjunto de elementos en el intervalo</label>
                      <input type="text" id="intervaloX" class="input">
                    </div>

                    <div class="col-md-12 div" id="RangoNu" style="display:none;">
                      <label for="iniRango">Numero inicial</label>
                      <input class="input" type="text" id="iniRango" value="">
                      <label for="finRango">Numero final</label>
                      <input class="input" type="text" id="finRango" value="">
                    </div>
                    <div class="col-md-12 div" id="autoIncrementForm" style="display:none;">
                      <div class="col-md-6">
                        <label for="iniRango">Numero inicial</label>
                        <input class="input" type="text" id="iniAuto">
                      </div>
                      <div class="col-md-6">
                        <label for="paso">Paso</label>
                        <input class="input" type="text" id="paso" value="">
                      </div>
                    </div>

                    <div class="col-md-12 div" id="intervaloFecha" style="display:none;">
                      <label for="FechaInicio">Fecha inico</label>
                      <input class="input" type="text" id="FechaInicio">
                      <label for="FechaFin">Fecha fin</label>
                      <input class="input" type="text" id="FechaFin">
                    </div>
                  </div>
                  <div class="col-md-6 col-md-offset-1" style="display:none;" id="tabla">
                    <p class="col-md-4">Nombre de la columna</p>
                    <p class="col-md-6">Tipo de dato</p>
                    <p class="col-md-1">TCampo</p>
                  </div>
                  <div class="col-md-6 col-md-offset-1" id="resultElement">
                  </div>
                  <br>
                  <div class="col-md-10 col-md-offset-1">
                    <div class="col-md-2">
                      <button type="button" id="btnGeneral" class="btn btn-success">General archivo</button>
                      <div style="display: none;" id="descargarArchivo">
                        <a id="referenciaA">Descargar archivo</a>
                      </div>
                    </div>
                    <div id="AnimacionCargando" class="col-md-12"  style="display:none;">
                      <p class="col-md-2" ALIGN=right>Creando archivo</p>
                      <div class="windows8 col-md-4">
                          <div class="wBall" id="wBall_1">
                          <div class="wInnerBall"></div>
                      </div>
                          <div class="wBall" id="wBall_2">
                          <div class="wInnerBall"></div>
                      </div>
                            <div class="wBall" id="wBall_3">
                            <div class="wInnerBall"></div>
                       </div>
                            <div class="wBall" id="wBall_4">
                            <div class="wInnerBall"></div>
                        </div>
                           <div class="wBall" id="wBall_5">
                           <div class="wInnerBall"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="" id="clonElement" style="display:none;">
                  <div id="capa" class="col-md-12">
                    <div class="espacio">
                    <p id="cNomColunm"    class="col-md-4"></p>
                    <p id="cTipoDato"     class="col-md-6"></p>
                    <p id="cTamanoCampo"  class="col-md-1"></p>
                    <div class="col-md-1">
                      <button type="button" class="btnCElemento btn btn-danger">
                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                      </button>
                    </div>
                    <input id="infoArray" style="display:none;"></input>
                  </div>
                  <hr>
                </div>
                <br>
              </div>
            </article>
				  </div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/base/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/base/bootstrap.min.js"></script>
  <script src="js/base/lumino.glyphs.js"></script>
  <script type="text/javascript" src="js/base/jquery-ui.js"></script>
  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/jquery.cecily.js"></script>
  <script type="text/javascript" src="js/generador/crearArchivos.js"></script>
 </body>
</html>
