  <div class="col-md-12">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="css/multi-select.css">
<section>
  <div class="col-md-12">
      <div class="col-md-6">
          <label for="departamento">Departamento</label>
          <select class="form-control" id="departamento">
            <option value="A0">Selecionar...</option>
          </select>
      </div>
      <div class="col-md-6">
          <label for="Ciudad">Ciudad</label>
          <select class="form-control" id="Ciudad">
            <option value="A0">Selecionar...</option>
          </select>
      </div>
      <div class="col-md-6">
          <label for="centroFormacion">Centro de formacion</label>
          <select class="form-control" id="centroFormacion">
            <option value="A0">Selecionar...</option>
          </select>
      </div>

      <div class="col-md-6" id="divDocumento">
          <label for="documento">Documento</label>
          <input type="text" id="documento" class="form-control">
      </div>
      <div class="col-md-6">
          <label for="nombre">Nombres</label>
          <input type="text" id="nombre" class="form-control">
      </div>
      <div class="col-md-6">
          <label for="apellidos">Apellidos</label>
          <input type="text" id="apellidos" class="form-control">
      </div>
      <div class="col-md-6">
          <label for="correo">Correo electronico</label>
          <input type="text" id="correo" class="form-control">
      </div>

      <div class="col-md-6">
        <label for="selectTipoUsuario">Tipo de usuario</label>
        <select class="form-control" id="selectTipoUsuario">
          <option value="A0">Selecionar...</option>
        </select>
        <!--button type="button" id="btnAgregarTipo" class="btn btn-info">Agregar</button>
        <div class="col-md-12" id="contenedorClon">

        </div-->
      </div>

      <div  style="display: none;" id="clonTipoUsuario">
        <div class="col-md-12 rowElement" >
          <label  class="labelTipoUsuario col-md-8"></label>
          <button type="button" class="btnELiminarTipo btn btn-danger col-md-4">Eliminar</button>
        </div>
      </div>


      <div class="col-md-6 col-md-offset-6" id="opcionesTesting" style="display: none;">
          <label for="SelectIpruebas" id="labelOpcion">Pruebas existentes</label>
          <select id="SelectIpruebas" class="itemSelect" multiple="multiple">
          </select>
      </div>

      <div class="col-md-6">
        <button type="button" id="btnguardar" class="btn btn-info">Guardar usuario</button>
      </div>

  </div>
  </section>

  <div class="col-md-10 col-md-offset-1" id="divTableUsuer">
      <table id="TableUser">
        <thead>
          <th>N°</th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Correo electronico</th>
          <th>Estado</th>
          <th></th>
          <th></th>
        </thead>
      </table>
  </div>
  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/datatables.js"></script>
  <script type="text/javascript" src="js/base/jquery-ui.js"></script>
  <script type="text/javascript" src="js/base/jquery.multi-select.js"></script>
  <script type="text/javascript" src="js/base/jquery.quicksearch.js"></script>
  <script type="text/javascript" src="js/administrador/usuario.js"></script>
</div>
