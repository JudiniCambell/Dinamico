<div class="col-md-12">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
<section>
  <div class="col-md-10">
    <div class="col-md-6">
      <label for="nombreHerramienta" class="col-md-12" >Nombre  de la herramienta</label>
      <input  type="text" id="nombreHerramienta" value="" class="form-control">
    </div>
    <div class="col-md-6">
      <label for="urlHerramienta" class="col-md-12">Url de  la herramienta</label>
      <input type="text" id="urlHerramienta" value="" class="form-control">
    </div>
    <div class="col-md-6">
      <label for="descripcionHerramienta" class="col-md-12">Descripcion de la herramienta</label>
      <textarea id="descripcionHerramienta" class="form-control"></textarea>
    </div>
    <div class="col-md-7">
      <br>
      <button type="button" id="btnAccion" class="btn btn-info">Agregar herramienta</button>
    </div>
  </div>
</section>

  <div class="">
    <table id="tableHerramientas" class="table table-striped">
      <thead>
        <th>N°</th>
        <th>Nombre</th>
        <th>Descricion</th>
        <th>Dirreccion web</th>
        <th>Modificar herramienta</th>
        <th>Eliminar herramienta</th>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/datatables.js"></script>
  <script type="text/javascript" src="js/administrador/herramientas.js"></script>
</div>
