<!DOCTYPE html>
<html id="formComplet">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token()}}"/>
  <link href="css/bootstrap.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  <title>Sena Testing Center-Gestionador</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" ><span>sena</span>testing center</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>{{Session::get('Nombre')}}<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
								<li><a  href="cerrar" id="1">Cerrar sesion</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
      <li class="active"><a class="labelS" id="0"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>Gestionador</a></li>
			<li><a class="labelS" id="0"><svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>Verificar solicitud</a></li>
		</ul>
	</div>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
						<div class="panel-heading" id="accordion"><svg id="useClassModulo" class="1"><use xlink:href="" id="useClass1Modulo"></use></svg><label id="titulo"></label></div>
					<div class="panel-body" id="contenido">

				  </div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/base/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/base/bootstrap.min.js"></script>
  <script src="js/base/lumino.glyphs.js"></script>
	<script type="text/javascript" src="js/base/jquery-ui.js"></script>
	<script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/datatables.js"></script>
  <script type="text/javascript" src="js/base/jquery.cecily.js"></script>
  <script type="text/javascript" src="js/gestionador/principalGestionador.js"></script>
 </body>
</html>
