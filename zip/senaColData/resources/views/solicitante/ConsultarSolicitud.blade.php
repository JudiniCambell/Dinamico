<div class="col-md-12">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  <div id="tablaDiv" class="" >
    <table id="tablaConsultarP" class="" >
      <thead>
        <th>N°</th>
        <th>Nombre</th>
        <th>Ticket</th>
        <th>Estado</th>
        <th>fecha inicial</th>
        <th>Fecha finalizacion</th>
        <th>Detalles</th>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div class="" id="detalleBase" style="display:none;">
      <label for="nomProducto">Nombre de la solicitud</label>
      <p id="nomProducto"></p>
      <label for="nTicked">Numero de ticked</label>
      <p id="nTicked"></p>
      <label for="descripcionSolicitud">Descripcion de la solicitud</label>
      <p id="descripcionSolicitud"></p>
      <label for="pruebaSolicitud">Prueba solicitada</label>
      <p id="pruebaSolicitud"></p>
      <label for="estadoSolicitud">Estado actual de la solicitud</label>
      <p id="estadoSolicitud"></p>
      <label for="fechaInicio">Fecha inicial de la solicitud</label>
      <p id="fechaInicio"></p>
      <div id="fechafinDiv" style="display:none;">
        <label for="fechafin">Fecha finalizacion de la solicitud</label>
        <p id="fechafin"></p>
      </div>
      <div  id="linkInformeRDIV" class="" style="display:none;">
        <label for="">Informe de resultados</label>
        <a id="linkInformeR" href="">Descargar</a>
      </div>
      <div id="informesParciales" style="display:none;">
        <div class="col-md-12">
          <p id="item" class="col-md-4"></p>
          <p id="fechaDocumento" class="col-md-4"></p>
          <a id="urlItem" href="" class="col-md-4">Descargar</a>
        </div>
      </div>
      <div id="acordionDiv" class="" style="display:none;">
          <div id="acordion"></div>
      </div>
      <div class=""  style="display:none;">
          <div class="" id="contenedorCorregui">
            <label for="" id="documentoRequerido"></label>
            <a href="" id="urlDocumentoRequerido"></a>
          </div>
      </div>
      <div class="" id="correguirSolicitudDIV" style="display:none;">
            <h3>Correguir solicitud</h3>
            <div id="contCorreguir">
              <label>Inconsistencias de la solicitud</label>
              <p id="Inconsistencias"></p>
              <form id="formS" class="">
                {{csrf_field()}}
                <div class="" id="formulacionModificarDiv">
                </div>
                <input type="text" name="arrArchivos" id="arrArchivosAntiguos"  style="display:none;">
                <input type="text" name="solicitud" id="solicitud"  style="display:none;">
                <button id="btnRealizarCorrecciones" type="button"  class="btn btn-info formMaldito">Realizar correccion</button>
              </form>
            </div>
      </div>
  </div>
  <div  id="itemsCorrecion" style="display:none;">
    <div class="col-md-12">
      <p id="item" class="col-md-4"></p>
      <a id="urlItem" href="" class="col-md-2">Descargar</a>
      <input id="archivoModificado" type="file" name="archivo[]">
    </div>
  </div>
  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/datatables.js"></script>
  <script type="text/javascript" src="js/solicitante/consultarS.js"></script>
</div>
