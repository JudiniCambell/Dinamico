$( document ).ready(function() {
  var element = document.getElementById('taCampo');
  var ob = new $.Luna("equipo");
  ob.Vivo("CrearArchivos");
  var selector = [], hilo = [], jso = [], data = [],datos=[],div= $(".div");
  console.log("Vivo??Archivos");

  jso[0] = {
    url:'../../tipoDatos',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: "null"
  };
  $("#FechaInicio").datepicker({
    changeMonth: true,
    changeYear: true
  });
  $("#FechaFin").datepicker({
    changeMonth: true,
    changeYear: true
  });
  $("#FechaInicio").change(function (){
      $("#FechaFin").datepicker("option", "minDate", $("#FechaInicio").val());
  });
  datos[0] = {nombre:"Select"};
  selector[0]= $("#selectTipDato");
  ajax(0);
  var tjson=0;
  element.removeAttribute("disabled");
  $("#selectTipDato").change(function (){
    for (var i = 0; i < div.length; i++) {
      var d =$(div[i]);
      d.hide();
    }
    var q = $("#selectTipDato").val();
    element.removeAttribute("disabled");
    if ((q==3)||(q==17)||(q==20)||(q==21)||(q==22)||(q==23)||(q==24)||(q==19)) {
      element.setAttribute('disabled', 'disabled');
    }
    if ((q==20)||(q==21)||(q==22)) {
      $("#intervaloN").hide();
      $("#DinamicoFecha").show()
      tjson=1;
    }else if(q==13){
      element.setAttribute('disabled', 'disabled');
      tjson=2;
      $("#DinamicoFecha").hide();
      $("#intervaloN").show();
    }else if((q==16)||(q==15)){
      $("#DinamicoRango").show();
      tjson=3;
    }else if(q==18){
      $("#autoIncrementForm").show();
      element.setAttribute('disabled', 'disabled');
      tjson=4;
    }else{
      tjson=0;
    }
  });
  var ccIntervalo=0;
  $("#CheckIntervalo").change(function(){
    if ($('#CheckIntervalo').prop('checked')) {
      $("#intervaloFecha").show();
      ccIntervalo=1;
    }else{
      $("#intervaloFecha").hide();
      ccIntervalo=0;
    }
  });
  var Checkrango =0;
  $("#Checkrango").change(function (){
    if ($('#Checkrango').prop('checked')) {
      element.setAttribute('disabled', 'disabled');
      $("#RangoNu").show();
      Checkrango=1;
    }else{
      $("#RangoNu").hide();
      element.removeAttribute("disabled");
      Checkrango=0;
    }
  });
  cc=0;cx=0;
  $("#btnAgregarElement").click(function(){
    if (cx==20) {
      $("#btnAgregarElement").prop('disabled',true);
    }
    if ($("#selectTipDato").val()=="A0") {
      alert("Selecione un tipo de dato");
    }else{
      if (cx==0) {
        $("#tabla").show();
      }
      var jsoData="";
      var clon  =   $("#clonElement").clone();
      var  text =  $("#selectTipDato option:selected").text();
      switch (tjson) {
        case 0:
          jsoData = jsoData+'$${"nombre":"'+ $("#nomColumn").val() +'","tipoDato":"'+ $("#selectTipDato").val() +'","tamanoCampo":"'+ $("#taCampo").val() +'"}';
          clon.find("#cTipoDato").text(text);
          break;
        case 1:
          jsoData =jsoData+ '$${"nombre":"'+ $("#nomColumn").val() +'","tipoDato":"'+$("#selectTipDato").val()+'","intervalo":"'+ ccIntervalo +'","FechaInicio":"'+ $("#FechaInicio").val() +'","fechaFinal":"'+$("#FechaFin").val()+'"}';
          if (ccIntervalo==1) {
            text = text + " Con intervalo entre: "+$("#FechaInicio").val()+ " - "+ $("#FechaFin").val();
          }else if(ccIntervalo==0){
            text =  text + " Sin intervalo. fecha al azar";
          }
          ccIntervalo=0;
          clon.find("#cTipoDato").text(text);
          break;
        case 2:
          jsoData = jsoData+'$${"nombre":"'+ $("#nomColumn").val() +'","tipoDato":"'+ $("#selectTipDato").val() +'","intervalo":"'+ $("#selectintervalo").val() +'","arrayInter":"'+$("#intervaloX").val()+'"}';
          text= "El tipo de intervalo es "+$("#selectintervalo option:selected").text()+" en el conjunto "+$("#intervaloX").val();
          clon.find("#cTipoDato").text(text);
          break;
        case 3:
          jsoData = jsoData+'$${"nombre":"'+ $("#nomColumn").val() +'","tipoDato":"'+ $("#selectTipDato").val() +'","tamanoCampo":"'+$("#taCampo").val()+'","inicioRango":"'+ $("#iniRango").val() +'","finRango":"'+$("#finRango").val()+'","tipoC":"'+Checkrango+'"}';
          if (Checkrango==1) {
           text= "El rango esta compuesto por el numero inicial: "+$("#iniRango").val()+" y el numero final: "+$("#finRango").val();
          }else if(Checkrango==0){
          text= "Sin rango y numero al azar";
          }
          Checkrango=0;
          clon.find("#cTipoDato").text(text);
          break;
        case 4:
          jsoData = jsoData+'$${"nombre":"'+ $("#nomColumn").val() +'","tipoDato":"'+ $("#selectTipDato").val() +'","iniAuto":"'+ $("#iniAuto").val() +'","paso":"'+$("#paso").val()+'"}';
          text= "El autoincrementable inicia con: "+$("#iniAuto").val()+ " realizando  pasos de: "+ $("#paso").val();
          clon.find("#cTipoDato").text(text);
          break;
      }
      clon.find("#capa").attr("class", "Elemen"+cc);
      clon.find("#cNomColunm").text($("#nomColumn").val());
      clon.find("#cTamanoCampo").text($("#taCampo").val());
      clon.find("#infoArray").attr("value",jsoData);
      clon.find("#infoArray").addClass("arrayInfo");
      clon.find(".btnCElemento").attr("id",cc);
      clon.children().appendTo($("#resultElement"));
      cc++;cx++;
      var select = $(".select");
      var input = $(".input");
      var check = $(".che");
      for (var i = 0; i < select.length; i++) {
        var se = $(select[i]);
        se.val('A0');
        se.change();
      }
      for (var i = 0; i < input.length; i++) {
        var inp =$(input[i]);
        inp.val("");
        inp.html("");
      }
      for (var i = 0; i < check.length; i++) {
        var che = $(check[i]);
        che.prop("checked", "");
      }
      for (var i = 0; i < div.length; i++) {
        var di = $(div[i]);
        di.hide();
      }
      element.removeAttribute("disabled");
    }
    cx= cx+1;
  });

  $(document).on('click','.btnCElemento',function(e){
    var c = "Elemen"+this.id;
    $("."+c).remove();
    cx=cx-1;
    if (cx==0) {
      $("#tabla").hide();
    }else if (cx<20) {
      $("#btnAgregarElement").prop('disabled', false);
    }
  });

  $("#btnGeneral").click(function(){
      if ($("#selectASalida").val()!="A0") {
        if ($("#nRegistros").val()>0) {
          if (cx>0) {
            btnAcep();
          }else{alert("Debe agregar almenos un elemento");}
        }else{alert("El numero de registro debe ser mayor a 0 ");}
      }else{alert("Selecione un tipo de archivo");}
  });
  function btnAcep(){
    var datJson =[];
    var arrayInfo = $(".arrayInfo");
    datJson = $("#selectASalida").val();
    datJson = datJson +"$$"+$("#nRegistros").val();
    var as =arrayInfo.length +3;
    datJson = datJson+"$$"+ as;
    for (var i = 0; i < arrayInfo.length; i++) {
      var tem =arrayInfo[i];
      datJson = (datJson + arrayInfo[i].value);
    }
    jso[2] = {
      url:'../../archivosCargar',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    datos[2] = {nombre:"Select"};
    selector[2]=$("#selectTipDato");
    $("#nRegistros").html("");
    $("#nRegistros").val("");
    $("#nomColumn").html();
    $("#nomColumn").val("");
    $("#taCampo").html("");
    $('#selectTipDato option[value="A0"]').attr("selected", "selected");
    $("#selectASalida").val("A0");
    $("#selectASalida").change();
    $("#btnGeneral").attr('disabled','true');
    $("#AnimacionCargando").show();
    ajax(2);
  }
  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          hilo[i].terminate();
          peticionCompleta(i);
      };
  };

  function peticionCompleta(i) {
    if (i==0) {
      var json = jQuery.parseJSON(data[i]);
      var j = Object.keys(json[0]);
      for (var t = 0;t < json.length; t++) {
        if (json[t][j[0]] !="12") {
          selector[0].append($('<option>', {
          value: json[t][j[0]],
          text: json[t][j[1]]
          }));
        }
      };
    }else if(i ==2){
      var daMen = data[i].split("$$");
      var men = "";
      if (daMen[0] == "true") {
          estado = ("success");
          men = daMen[1];
          $("#descargarArchivo").show();
          $("#referenciaA").attr('href',daMen[2]);
      } else {
          estado = ("error");
          men = daMen[1];
      }
      $("#btnGeneral").prop('disabled', false);
      $("#AnimacionCargando").hide();
      $.notify(men, estado);
    }
  }
});
