jQuery.Luna = function (Datos, selector) {
    this.cor = ("El gato lopez");
    this.Nombre = selector;
    this.Cons = Datos;
    this.contador = 0;
    this.bus = [1, 2, 3, 4];
    this.setCons = function (dato) {
        this.Cons = dato;
    };
    this.Relativo = function () {
        return "http://192.168.0.12/Dinamico/app/senaColData/public/";
    };
    this.Vivo = function (mensaje) {
        console.log("SenaTestingCenterF??Cecil." + mensaje + " ");
        //logger.disableLogger();Dinamico//public/
        //    return "http://localhost:8080/proyectos/Dinamico/senaColData/public/";
    };
    this.limpiarSelector = function (selector) {
        selector.empty().append('<option value="A0">selecione...</option>');
    };
    this.TablaEspa = function (datos) {
        datos.DataTable({
            language: {
                paginate: {
                    first: "Primera",
                    previous: "Anterior",
                    next: "Siguiente",
                    last: "Anterior"
                },
                processing: "Cargando datos...",
                lengthMenu: "Mostrar _MENU_ " + this.Cons,
                info: "Se encontaron _TOTAL_ " + this.Cons,
                infoEmpty: "Mostradas 0 de _MAX_ entradas",
                "infoFiltered": "(filtrada a partir de  _MAX_ registro)",
                infoPostFix: "",
                loadingRecords: "Cargando...",
                "zeroRecords": "Ningun " + this.Cons + " encontrada",
                emptyTable: "No hay ningun " + this.Cons,
                search: "Buscar:"
            }
        });
    };
    this.mostrarVentana = function (selector) {
        selector.show();
    };
    this.ocultarVentana = function (selector) {
        selector.hide();
    };
    this.ajax = function (datos, selector) {
        $.ajax({
            url: datos.url,
            type: 'POST',
            async: true,
            cache: false,
            datatype: 'json',
            data: datos,
            success: function (json) {
                cargarTabla(json, selector, datos);
            },
            error: function () {
                alert = ("Disculpa, pero existe un error al cargar datos del servidor :/");
            }
        });
    };
    this.cargarTabla = function (json, selector, datos) {
        cargarTabla(json, selector, datos);
    };
    function cargarTabla(json, selector, datos) {
        try {
            var c = 1;
            switch (datos.nombre) {
                case "MultiSelect":
                    if (datos.compuesto == true) {
                        var j = Object.keys(json[0]);
                        var opcion = "";
                        selector.empty("");
                        for (var i = 0; i < json.length; i++) {
                            if (json[i].tipo == true) {
                                opcion = "<option value=" + json[i][j[0]] + " disabled='disabled' selected>" + json[i][j[1]] + "</option>";
                            } else if (json[i].tipo == false) {
                                opcion = "<option value=" + json[i][j[0]] + ">" + json[i][j[1]] + "</option>";
                            } else {
                                opcion = "<option value=" + json[i][j[0]] + " selected>" + json[i][j[1]] + "</option>";
                            }
                            selector.append(opcion);
                        }
                    } else {
                        selector.empty("");
                        var jso = jQuery.parseJSON(json);
                        var j = Object.keys(jso[0]);
                        for (var i = 0; i < jso.length; i++) {
                            var opcion = "<option value=" + jso[i][j[0]] + ">" + jso[i][j[1]] + "</option>";
                            selector.append(opcion);
                        }
                    }
                    selector.multiSelect('refresh');
                    if (datos.opt == "Div") {
                        selector.multiSelect('deselect_all');
                    }
                    break;

                case "Select":
                    if (datos.opt == "limpiaM") {
                        selector.empty();
                    } else if (datos.opt == "limpiaN") {
                        selector.empty().append("<option value='A0'>Selecionar...</option>");
                    }
                    var jso = jQuery.parseJSON(json);
                    var j = Object.keys(jso[0]);
                    for (var i = 0; i < jso.length; i++) {
                        selector.append($('<option>', {
                            value: jso[i][j[0]],
                            text: jso[i][j[1]]
                        }));
                    }
                    break;
                case "AutoComplet":
                    var jso = jQuery.parseJSON(json);
                    var j = Object.keys(jso[0]);
                    var s = [];
                    for (var i = 0; i < jso.length; i++) {
                        s.push(jso[i][j[1]]);
                    }
                    selector.autocomplete({
                        source: s,
                        minChars: 2
                    });
                    break;
                case "Itemsprueba":
                    var conArchivos = "Elemento$%s";
                    var jso = jQuery.parseJSON(json);
                    for (var i = 0; i < jso.length; i++) {
                        if (i == 0) {
                            $("#resultItems").append("<label>Items requeridos de la prueba</label>");
                        }
                        conArchivos = conArchivos + "," + jso[i].nombre;
                        var clon = selector.clone();
                        clon.find(".Caso").attr('id', 'archivos' + i);
                        clon.find(".Caso").text(jso[i].nombre);
                        clon.find(".FileInput").attr('id', 'archivo' + i);
                        clon.find(".FileInput").attr('name', 'archivo[]');
                        clon.find(".FileInput").addClass('File');
                        clon.children().appendTo($("#resultItems"));
                    }
                    $("#ItemspruebasLabel").attr('value', conArchivos);
                    $("#contador").val(i);
                    $("#count").text(i);
                    break;
                case "verificarSolicitud":
                    var jsoli = jQuery.parseJSON(json);
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        Datos = jsoli[i].idSolicitud + "$$" + jsoli[i].idP + "$$" + jsoli[i].nombreSolicitud + "$$" + jsoli[i].descripcionSolicitud + "$$" + jsoli[i].prueba + "$$" + jsoli[i].ticked;
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombreSolicitud,
                            jsoli[i].ticked,
                            jsoli[i].prueba,
                            jsoli[i].fechaSolicitud,
                            "<button  type='button' id='" + Datos + "' class='btn btn-info btnVerificarSG'>" + datos.btn + "</button>"
                        ]);
                    }
                    break;
                case "ConsutarSolicitud":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].idSolicitud + "$$" + jsoli[i].idEstado;
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombreSolicitud,
                            jsoli[i].ticked,
                            jsoli[i].nombreEstado,
                            jsoli[i].fechaSolicitud,
                            jsoli[i].fechaFinalizacion,
                            "<button  type='button' id='" + dat + "'class='btn btn-info btnConsultar'>Detalles</button>"
                        ]);
                    }
                    break;
                case "consultarSolcitudesListas":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].idSolicitud + "$$" + jsoli[i].idP + "$$" + jsoli[i].nombreSolicitud + "$$" + jsoli[i].descripcionSolicitud + "$$" + jsoli[i].prueba + "$$" + jsoli[i].descripcionPrueba;
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombreSolicitud,
                            jsoli[i].descripcionSolicitud,
                            jsoli[i].prueba,
                            "<button  type='button' id='" + jsoli[i].idSolicitud + "'class='btn btn-info btnDevolver'>Devolver</button>",
                            "<button  type='button' id='" + dat + "'class='btn btn-info btnRealizarPrueba'>Realizar</button>"
                        ]);
                    }
                    break;
                case "herramientas":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].idSolicitud + "$$" + jsoli[i].idP + "$$" + jsoli[i].nombreSolicitud + "$$" + jsoli[i].descripcionSolicitud + "$$" + jsoli[i].prueba + "$$" + jsoli[i].descripcionPrueba;
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombreHerramienta,
                            jsoli[i].descripcionHerramienta,
                            "<a href=" + jsoli[i].urlHerramienta + ">Ir a la herramienta"
                        ]);
                    }
                    break;
                case "HerramientasConsulta":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].idHerramienta + "$$" + jsoli[i].nombreHerramienta + "$$" + jsoli[i].descripcionHerramienta + "$$" + jsoli[i].urlHerramienta;
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombreHerramienta,
                            jsoli[i].descripcionHerramienta,
                            "<a href=" + jsoli[i].urlHerramienta + ">Ir a la herramienta",
                            "<button  type='button' id='" + dat + "'class='btn btn-info btnModificar'>Modificar</button>",
                            "<button  type='button' id='" + jsoli[i].idHerramienta + "'class='btn btn-danger btnEliminarHerramienta'>Eliminar</button>"
                        ]);
                    }
                    break;
                case "PruebasConsulta":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].idPrueba + "$$" + jsoli[i].nombre + "$$" + jsoli[i].descripcion + "$$" + jsoli[i].estado;
                        if (jsoli[i].estado == 0) {
                            es = "Habilitado";
                        } else {
                            es = "Deshabilitado";
                        }
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombre,
                            jsoli[i].descripcion,
                            es,
                            "<button  type='button' id='" + jsoli[i].idPrueba + "'class='btn btn-info btnAgregarItems'>Agregar items</button>",
                            "<button  type='button' id='" + dat + "'class='btn btn-info btnModificar'>Modificar</button>",
                            "<button  type='button' id='" + jsoli[i].idPrueba + "'class='btn btn-danger btnDeshabilitar'>Deshabilitar</button>"
                        ]);
                    }
                    break;
                case 'plantillasPrueba':
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].idPlantilla + "$$" + jsoli[i].nombrePlantilla + "$$" + jsoli[i].fechaVigencia + "$$" + jsoli[i].formatoPlantilla + "$$" + jsoli[i].estado;
                        var es = "";
                        if (jsoli[i].estado == 0) {
                            es = "Habilitado";
                        } else {
                            es = "Deshabilitado";
                        }
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombrePlantilla,
                            "<a href='descargar?base=pruebas/plantillas/&file=" + jsoli[i].formatoPlantilla + "'>Descargar",
                            jsoli[i].fechaVigencia,
                            es,
                            //"<button  type='button' id='"+dat+"'class='btn btn-info btnModificarPlantilla'>Modificar</button>",
                            "<button  type='button' id='" + jsoli[i].idPlantilla + "'class='btn btn-danger btnDeshabilitarPlantilla'>Deshabilitar</button>"
                        ]);
                    }
                    break;
                case "itemsPrueba":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < jsoli.length; i++) {
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombre,
                            jsoli[i].descripcion
                        ]);
                    }
                    break;
                case "itemsPruebaAñadir":
                    selector.dataTable().fnClearTable();
                    for (var i = 0; i < json.length; i++) {
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            json[i].nombre,
                            json[i].descripcion,
                            '<button  type="button" id=' + json[i].idP + '  class="btn btn-danger btnEliminarItems">Eliminar</button>'
                        ]);
                    }
                    break;
                case "ItemsPruebaSelect":
                    var jso = jQuery.parseJSON(json);
                    $("#resultItemsP").empty();
                    for (var i = 0; i < jso.length; i++) {
                        var clonIten = selector.clone();
                        clonIten.find("#NombreItem").text(jso[i].nombre);
                        clonIten.find("#checkBox").attr('name', 'check' + i);
                        clonIten.find("#checkBox1").attr('name', 'check' + i);
                        clonIten.find("textArea").attr('name', 'textA' + i);
                        clonIten.find("#items").attr('name', 'item' + i);
                        clonIten.find("#items").val(jso[i].idItem);
                        clonIten.children().appendTo($("#resultItemsP"));
                    }
                    $("#numItems").val(jso.length);
                    break;
                case "RequerimientoSolicitud":
                    var jso = jQuery.parseJSON(json);
                    $("#itemsPrueba").empty().append("<div class='col-md-12'><label class='col-md-8'>Documentos de la solicitud</label><label class='col-md-4'>Cumple</label</div>");
                    for (var i = 0; i < jso.length; i++) {
                        if (jso[i].descripcionDocumento == "Elemento$%s") {
                            $("#labelElemetoDoc").text("Elemento de la solicitud");
                            $("#ElemetoDocDecargar").attr('href', 'descargar?base=solicitudes/documentosSolicitud/&file=' + jso[i].urlArchivo);
                        } else {
                            var clon = selector.clone();
                            clon.find("#laRequerimitentos").text(jso[i].descripcionDocumento);
                            clon.find("#DecargarD").attr('href', ' descargar?base=solicitudes/documentosSolicitud/&file=' + jso[i].urlArchivo);
                            clon.find("#siOpcionItem").attr('name', 'vaItem' + i);
                            clon.find("#siOpcionItem").attr('value', jso[i].documento);
                            clon.find("#noOpcionItem").attr('name', 'vaItem' + i);
                            clon.find("#noOpcionItem").attr('value', jso[i].documento);
                            clon.children().appendTo($("#itemsPrueba"));
                        }
                    }
                    break;
                case "RequerimientoSolicitudExperto":
                    var jso = jQuery.parseJSON(json);
                    $("#itemsPruebas").empty().append("<div class='col-md-12'><label class='col-md-8'>Documentos de la solicitud</label><label class='col-md-4'>Cumple</label</div>");
                    for (var i = 0; i < jso.length; i++) {
                        if (jso[i].descripcionDocumento == "Elemento$%s") {
                        } else {
                            var clon = selector.clone();
                            clon.find("#laRequerimitentos").text(jso[i].descripcionDocumento);
                            clon.find("#DecargarD").attr('href', ' descargar?base=solicitudes/documentosSolicitud/&file=' + jso[i].urlArchivo);
                            clon.find("#siOpcionItem").attr('name', 'vaItem' + i);
                            clon.find("#siOpcionItem").attr('value', jso[i].documento);
                            clon.find("#noOpcionItem").attr('name', 'vaItem' + i);
                            clon.find("#noOpcionItem").attr('value', jso[i].documento);
                            clon.children().appendTo($("#itemsPruebas"));
                        }
                    }
                    break;
                case "consultaUsuarios":
                    var jsoli = jQuery.parseJSON(json), dat = "";
                    selector.dataTable().fnClearTable();
                    estado = "Deshabilitado";
                    for (var i = 0; i < jsoli.length; i++) {
                        dat = jsoli[i].usuario + "$$" + jsoli[i].nombres + "$$" + jsoli[i].apellidos + "$$" + jsoli[i].correoElectronico + "$$" + jsoli[i].centro + "$$" + jsoli[i].ciudad + "$$" + jsoli[i].departamento;
                        if (jsoli[i].estado == "1") {
                            estado = "Habilitado";
                        } else if (jsoli[i].estado == "2") {
                            estado = "Deshabilitado";
                        } else {
                            estado = "Inactivo";
                        }
                        table = selector.dataTable().fnAddData([
                            i + 1,
                            jsoli[i].nombres,
                            jsoli[i].apellidos,
                            jsoli[i].correoElectronico,
                            estado,
                            "<button  type='button' id='" + dat + "'class='btn btn-info btnModificarUsuario'>Modificar</button>",
                            "<button  type='button' id='" + jsoli[i].usuario + "'class='btn btn-danger btnDeshabilitarUsuario'>Deshabilitar</button>"
                        ]);
                    }
                    break;
                case "RequerimientoSolicitud1":
                    var jso = jQuery.parseJSON(json);
                    $("#itemsPrueba").empty().append("<label >Documentos de la solicitud</label>");
                    for (var i = 0; i < jso.length; i++) {
                        if (jso[i].descripcionDocumento == "Elemento$%s") {
                            jso[i].descripcionDocumento = "Elemento de la solicitud";
                        }
                        var clon = selector.clone();
                        clon.find("#laRequerimitentos").text(jso[i].descripcionDocumento);
                        clon.find("#DecargarD").attr('href', 'descargar?base=solicitudes/documentosSolicitud/&file=' + jso[i].urlArchivo);
                        clon.children().appendTo($("#itemsPrueba"));
                    }
                    break;
                case "solicitudInformesPrevios":
                    var countInforPre = 0;
                    $("#acordion").accordion('destroy');
                    var jso = jQuery.parseJSON(json);
                    $("#acordion").html("");
                    if ((datos.estado == 8) || (datos.estado == 6) || (datos.estado == 3)) {
                        $("#acordion").append("<h3>Informes previos</h3><div id='contPrevios' class='contPrevios'></div>");
                        if ((datos.estado == 8) || (datos.estado == 6)) {
                            var clonDiv = $("#correguirSolicitudDIV").clone();
                            clonDiv.children().appendTo($("#acordion"));
                        }
                        if (jso.length < 1) {
                            $(".contPrevios").append("<label>No hay ningun informe previo </label>");
                        }
                        for (var i = 0; i < jso.length; i++) {
                            var clon = $("#informesParciales").clone();
                            clon.find("#urlItem").attr('href', 'descargar?base=solicitudes/documentosTesting/&file=' + jso[i].urlArchivo);
                            clon.find("#fechaDocumento").text(jso[i].fechaDocumento);
                            clon.find("#item").text(jso[i].descripcionDocumento);
                            if (jso[i].fktipoDocument == 2) {
                                clon.children().appendTo($(".contPrevios"));
                            }
                        }
                    } else {
                        var seccion = "<h3>Informes previos</h3><div id='contPrevios'></div><h3>Informes finales</h3><div id='contInformesfinales'></div>";
                        selector.append(seccion);
                        for (var i = 0; i < jso.length; i++) {
                            var clon = $("#informesParciales").clone();
                            clon.find("#fechaDocumento").text(jso[i].fechaDocumento);
                            clon.find("#item").text(jso[i].descripcionDocumento);
                            if (jso[i].fktipoDocument == 2) {
                                clon.find("#urlItem").attr('href', 'descargar?base=solicitudes/documentosTesting/&file=' + jso[i].urlArchivo);
                                clon.children().appendTo($("#contPrevios"));
                                countInforPre++;
                            } else if (jso[i].fktipoDocument == 5) {
                                clon.find("#urlItem").attr('href', 'descargar?base=solicitudes/documentosFinales/&file=' + jso[i].urlArchivo);
                                clon.children().appendTo($("#contInformesfinales"));
                            }
                        }
                        if (countInforPre < 1) {
                            $("#contPrevios").append("<label> No hay ningun informe previo </label>");
                        }
                    }

                    $("#acordion").accordion({ heightStyle: 'content' });
                    break;
                case "itemSolicitudSolicitante":
                    var jso = jQuery.parseJSON(json);
                    var arrArchivosAntiguos = [];
                    $("#formulacionModificarDiv").empty().append("<label><h4> Documentos con alguna falla</h4></label>");
                    for (var i = 0; i < jso.length; i++) {
                        var clon = $("#itemsCorrecion").clone();
                        clon.find("#urlItem").attr('href', 'descargar?base=solicitudes/documentosSolicitud/&file=' + jso[i].urlArchivo)
                        clon.find("#item").text(jso[i].descripcionDocumento);
                        clon.children().appendTo($("#formulacionModificarDiv"));
                        arrArchivosAntiguos.push(jso[i].urlArchivo + "$$" + jso[i].idCertificadoSolicitud + "$$" + jso[i].descripcionDocumento);
                    }
                    $("#solicitud").attr('value', jso[0].fkSolicitud);
                    $("#arrArchivosAntiguos").attr('value', arrArchivosAntiguos);
                    break;
            }
        } catch (error) {
            console.log(error.message);
        }
    }
    var logger = function () {
        var oldConsoleLog = null;
        var pub = {};
        pub.disableLogger = function disableLogger() {
            oldConsoleLog = console.log;
            window['console']['log'] = function () { };
        };
        return pub;
    }
        ();
};
