$(document).ready(function(){
  var obsession = {"tipoUsuario":"12"};
  var selector = [], hilo = [], jso = [], data = [],datos=[],datJson="";
  var ob = new $.Luna("pricipalGestionador");
  ob.Vivo("pricipalGestionador");

  datos[0]={opcion:0,icon:"checkmark",label:"#stroked-checkmark",titulo:"Verificar solicitud"};
  datJson='{"option":"'+0+'","tip":"'+obsession.tipoUsuario+'"}';
  jso[1] = {
    url: '../../linkGestor',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: datJson
  };
  ajax(1);

  $(document).on('click','.labelS',function(e){
    var s = this.id;
    $("#btnIngresar").prop('disabled', true);
    switch (s) {
      case '0':
         datos[0]={opcion:s,icon:"checkmark",label:"#stroked-checkmark",titulo:"Verificar solicitud"};
        break;
      case '1':
         datos[0]={opcion:s};
        break;
    }
    datJson='{"option":"'+s+'","tip":"'+obsession.tipoUsuario+'"}';
    jso[1] = {
      url:'../../linkGestor',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          hilo[i].terminate();
          peticionCompleta(i);
      };
  };

  function peticionCompleta(i) {
      if(i==1){
        if (datos[0].opcion=='1') {
          $("#formComplet").html("");
          $("#formComplet").html(data[i]);
        }else{
          $("#contenido").html("");
          $("#contenido").html(data[i]);
          $("#useClassModulo").attr('class','');
          $("#useClassModulo").attr('class','glyph stroked '+datos[0].icon+'');
          $("#useClass1Modulo").attr('xlink:href',datos[0].label);
          $("#titulo").text(datos[0].titulo);
        }
      }
    }

});
