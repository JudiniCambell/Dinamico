<!DOCTYPE html>
<html id="formComplet">
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token()}}"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sena Testing Center-Cambiar Contraseña</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<link href="css/jquery-ui.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" ><span>sena</span>testing center</a>

			</div>
		</div>
	</nav>

	<div class="col-sm-11 col-md-11 col-lg-11  main">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
						<div class="panel-heading" id="accordion"><svg class="glyph stroked male user"><use xlink:href="#stroked-male-user" id="useClass1Modulo"></use></svg><label id="titulo">Cambiar contraseña</label></div>
					<div class="panel-body" id="contenido">
							<div class="col-md-12">
								<input type="hidden" id="token" value="{{$token}}">
								<input type="hidden" id="user" value="{{$usu}}">
								<div class="col-md-6 col-md-offset-2">
									<label for="usu" class="col-md-12">Usuario</label>
									<input type="text" id="usu" value="" class="form-control">
								</div>
								<div class="col-md-6 col-md-offset-2">
									<label for="usu" class="col-md-12">Nueva contraseña</label>
									<input type="text" id="con1" value="" class="form-control">
								</div>
								<div class="col-md-6 col-md-offset-2">
									<label for="usu" class="col-md-12">Confirmar contraseña</label>
									<input type="text" id="con" value="" class="form-control">
								</div>
								<div class="col-md-6 col-md-offset-2">
									<button type="button" id="btnguardarCon" class="btn btn-info">Cambiar contraseña</button>
								</div>
							</div>
				  </div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/base/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/base/bootstrap.min.js"></script>
  <script src="js/base/lumino.glyphs.js"></script>
	<script type="text/javascript" src="js/base/jquery-ui.js"></script>
	<script type="text/javascript" src="js/base/notify.js"></script>
	<script type="text/javascript" src="js/base/jquery.cecily.js"></script>
  <script type="text/javascript" src="js/incio/cammbio.js"></script>
</body>
</html>
