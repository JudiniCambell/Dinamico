<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sena Testing Center</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
</head>
<body>
	<div class="col-md-12">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Inicio de sessión</div>
				<div class="panel-body">
          <form  action="pricipal" method="POST">
						<!--prueba-->
            <fieldset>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div>
              <label for="user" class="col-md-12">Usuario</label>
              <div >
                <input name="usu" type="text" id="user" value="" class="form-control" placeholder="Ingrese su documento de identificacion">
              </div>
              <label for="pass" class="col-md-12">Contraseña</label>
              <div >
                <input name="pass" type="password" id="pass" value="" class="form-control" placeholder="Ingrese su contraseña">
              </div>
              <div class="">
                  <button type="submit" id="btnIngresars" class="btn btn-primary">Ingresar</button>
              </div>
            </div>
            </fieldset>
          </form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/base/jquery-1.11.1.min.js"></script>
	<script src="js/base/bootstrap.min.js"></script>

  <script type="text/javascript" src="js/incio/session.js"></script>
</body>
</html>
