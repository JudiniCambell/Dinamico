<div class="col-md-12">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    <div class="" id="EspacioTable">
      <table class="table table-striped" id="tablaVerificarLista">
        <thead>
            <th>N°</th>
            <th>Nombre</th>
            <th>Ticked</th>
            <th>Prueba solicitada</th>
            <th>Fecha de solicitud</th>
            <th>Verificar solicitud</th>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div class="col-md-12" id="formVerifica" style="display: none;">
      <label for="NombreP"  class="col-md-12">Nombre del producto</label>
      <p id="NombreP" class="col-md-12"></p>
      <label for="DescripcionP" class="col-md-12">Descripcion del producto</label>
      <p id="DescripcionP" class="col-md-12"></p>
      <label for="labelElemetoDoc" class="col-md-12">Producto de la solicitud</label>
      <p id="labelElemetoDoc" class="col-md-4"></p>
      <a id="ElemetoDocDecargar" href="" class="col-md-8">Descargar</a>

      <div class="col-md-12">
          <div class="col-md-6" id="itemsPrueba">

          </div>
          <div class="col-md-6">
            <p id="RequePrueba"></p>
            <select id="RequerimientoSolicitud" class="form-control" multiple>

            </select>
          </div>
      </div>
    </div>

    <div id="itemsDocumetosSolicitud" style="display: none;">
      <div class="col-md-12">
        <p id="laRequerimitentos" class="col-md-4"></p>
        <a id="DecargarD" href="" class="col-md-4">Descargar</a>
        <form class="col-md-4">
            <input type="radio" class="radioItem" name="" value="0" id="siOpcionItem">Si
            <input type="radio" class="radioItem" name="" value="1" id="noOpcionItem">No
        </form>
      </div>
    </div>

    <div id="SolicitudBuena" class="" style="display: none;">
      <label for="">Asignar un encargado de pruebas</label>
      <div class="">
        <div class="col-md-6">
          <select class="form-control" id="selectTestingP">
            <option value="A0">Selecionar...</option>
          </select>
        </div>
        <div class="col-md-6">
          <label id="VSolicitud" class="col-md-12"></label>
          <select class="form-control" id="pruebas" multiple>

          </select>
        </div>
      </div>
    </div>
    <div id="SolicitudFalla" class="" style="display: none;">
      <label class="col-md-12">Inconsistencias de la solicitud</label>
      <div class="col-md-6">
          <textarea id="textAreaFlaso" class="form-control"></textarea>
      </div>
    </div>
    <div class="col-md-12" style="display: none;" id="divBtn">
        <button type="button" id="btnEnviar" class="btn btn-info">Enviar</button>
    </div>
    <script type="text/javascript" src="js/base/notify.js"></script>
    <script type="text/javascript" src="js/base/datatables.js"></script>
    <script type="text/javascript" src="js/gestionador/verificarSoli.js"></script>
</div>
