<html>
  <style type="text/css" media="screen">
    body {
    	background: #f1f4f7;
    	padding-top: 50px;
    	color: #5f6468;
    }
    p {
    	color: #777;
    }
    a, a:hover, a:focus {
    	color: #30a5ff;
    }
    h1, h2, h3, h4, h5, h6 {
    	color: #5f6468;
    }
    h1 {
    	font-weight: 300;
    	font-size: 40px;
    }

    h2 {
    	font-weight: 300;
    	margin-bottom: 20px;
    }
    h3, h4 {
    	font-weight: 300;
    }
  </style>

  <body>
    <h2>Bienvenido a SenaColData</h2>
    <p>Estimado usuario: {{$nombre}} </p>
    <br>
    <p>Su usuario en el aplicativo es: {{$documento}}</p>
    <p>Para ingresar en el aplicativo debe cambiar su contraseña</p>
    <br>
    <a class="btn btn-info" href={{$datSolicitud}}>Cambiar contraseña</a>
  </body>
</html>
