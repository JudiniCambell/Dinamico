<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
class linksGestionador extends Controller{
  function BuscarModulo(){
    try {
      $res =$_GET['data'];
      $json = json_decode($res);
      if ($json->tip==12) {
        switch ($json->option) {
          case 0:
              return view('gestionador/verificarSolicitud');
            break;
          case 1:
              $this->cerraT();
              return view('incio/index');
            break;
          default:
              $this->cerraT();
              return view('incio/index');
            break;
          }
      }else{
        $this->cerraT();
        return view('incio/index');
      }
    } catch (Exception $e) {
      $this->cerraT();
      return view('incio/index');
    }
  }
  function cerraT(){
    Session::forget('idTUsuario');
    Session::forget('idUser');
    Session::forget('idCentro');
  }
}
