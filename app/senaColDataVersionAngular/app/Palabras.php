<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Palabras extends Model
{
  protected $table="Palabras";
  protected $primaryKey="idPalabra";
  public $timestamps=false;
}
