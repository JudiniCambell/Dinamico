<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apellido extends Model
{
  protected $table="Apellido";
  protected $primaryKey="idApellido";
  public $timestamps=false;
}
