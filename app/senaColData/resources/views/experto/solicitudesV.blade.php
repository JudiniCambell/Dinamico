<div class="col-md-12">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link href="css/theme.css" media="all" rel="stylesheet" type="text/css"/>
  <div class="" id="divTable">
    <table id="tablaRealizarP" class="table table-striped" >
      <thead>
        <th>N°</th>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Prueba solicitada</th>
        <th>Devolver solicitud</th>
        <th>Realizar prueba</th>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div id="divRealizar" class="col-md-12" style="display: none;">
    <div class="col-md-12">
      <div class="col-md-6">
        <label for="NombrePrueba">Nombre de la prueba</label>
        <p id="NombrePrueba"></p>
        <label for="DescripcionPrueba" >Descripcion  de la prueba</label>
        <p id="DescripcionPrueba" ></p>
      </div>
      <div class="col-md-6">
        <label for="NombreSolicitud">Nombre de la solicitud</label>
        <p id="NombreSolicitud" ></p>
        <label for="DescripcionSolicitud" >Descripcion de la solicitud</label>
        <p id="DescripcionSolicitud" ></p>
      </div>
    </div>
    <div class="col-md-12">
        <div id="Acordion">
            <h3>Documentos de la solicitud</h3>
            <div>
              <div id="itemsPrueba">

              </div>
            </div>
            <h3>Herramientas disponibles para esta prueba</h3>
            <div>
              <table id="tableHerramientasP">
                <thead>
                  <th>N°</th>
                  <th>Nombre</th>
                  <th>Descripcion</th>
                  <th>Dirrecion web</th>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <h3>Resultados parciales</h3>
            <div>
              <label for="SelectPlantilla" class="col-md-12">formato</label>
              <div class="col-md-5">
                <select class="form-control" id="SelectPlantilla" class="form-control">
                  <option value="A0">Seleciona...</option>
                </select>
              </div>
              <div id="labelsPlantillas" class="col-md-12" style="display: none;">
                <label class="col-md-2">Nombre</label>
                <label class="col-md-3">Descripcion</label>
                <label class="col-md-2">Descargar plantilla</label>
                <label class="col-md-4">Nuevo Archivo</label>
                <label class="col-md-1">Eliminar</label>
              </div>
              <button type="button" class="btn btn-info" id="btnAgregarPlantilla">Agregar</button>
              <form class="" id="formresultadoParcial">
                <div class="col-md-12" id="formEnvioPlantillas">

                </div>
                {{csrf_field()}}
                <input type="hidden" name="solicitud" class="solicitud" value="">
                <input type="hidden" name="arrayPlantilla" id="arrayPlantilla" value="">
                <button  type="submit" id="btnRealizarPrueba" class="btn btn-info">Enviar resultados parciales</button>
              </form>
            </div>
            <h3>Realizar calificacion final</h3>
            <div>
              <form class="" id="formResultadoFinal">
                {{csrf_field()}}
                <div class="col-md-12">
                    <label class="col-md-5">Nombre del item</label>
                    <label class="col-md-2">¿Aprobo?</label>
                    <label class="col-md-5">Observaciones</label>
                    <div id="resultItemsP">

                    </div>
                    <input type="hidden" name="numItems" id="numItems" value="">
                </div>
                <label for="">Reporte de la prueba(Agrega uno o varios reportes) </label>
                <input id="file-es" name="archivo[]" type="file" multiple>
                <input type="hidden" name="solicitud" class="solicitud" value="">
                <br>
                <button  type="submit" id="btnRealizarPrueba" class="btn btn-info">Realizar calificacion</button>
              </form>
            </div>
        </div>
      </div>
  </div>

  <div id="aceptarPlantillaClon" style="display: none;">
    <div class="contenedor col-md-12 con">
      <p for="archivoPlatilla" id="nombrePlantilla" class="col-md-2"></p>
      <p for="archivoPlatilla" id="desPlantilla" class="col-md-3"></p>
      <a  id="linkDescarga" href="" class="col-md-2">Descargar plantilla</a>
      <input type="file" id="archivoPlatilla" name="archivo[]" class="col-md-4">
      <button type="button" id="btnEliminar" class="col-md-1 btn btn-default btnElimplan">X</button>
    </div>
  </div>
  <div id="itemsPruebaClon"  style="display: none;">
    <div class="con col-md-12">
      <p id="NombreItem" class="col-md-5"></p>
      <div class="col-md-2">
        <input type="radio" id="checkBox" value="1" class="checkFull">Si
        <input type="radio" id="checkBox1" value="0" class="checkFull">No
      </div>
      <div class="col-md-5">
          <textarea id="textArea" class="form-control"></textarea>
      </div>
      <input type="hidden"   id="items" value="">
    </div>
  </div>
  <div id="itemsDocumetosSolicitud" style="display: none;">
    <div class="col-md-12">
      <p id="laRequerimitentos" class="col-md-4"></p>
      <a id="DecargarD" href="" class="col-md-8">Descargar</a>
    </div>
  </div>
  <div id="devolver" class="" style="display: none;">
    <!--p id="labelElemetoDoc" class="col-md-4"></p>
    <a id="ElemetoDocDecargar" href="" class="col-md-8">Descargar</a-->
    <div id="itemsPruebas">
    </div>
    <div class="col-md-6">
        <label class="col-md-12">Inconsistencias de la solicitud</label>
        <textarea id="textAreaFlaso" class="form-control"></textarea>
        <br>
    </div>
    <div class="col-md-12">
      <button class="btn btn-info" type="button" id="btnDevolver">Devolver solicitud</button>
    </div>
  </div>


  <div id="itemsDocumetossk" style="display: none;">
    <div class="col-md-12">
      <p id="laRequerimitentos" class="col-md-4"></p>
      <a id="DecargarD" href="" class="col-md-4">Descargar</a>
      <form class="col-md-4">
          <input type="radio" class="radioItem" name="" value="0" id="siOpcionItem">Si
          <input type="radio" class="radioItem" name="" value="1" id="noOpcionItem">No
      </form>
    </div>
  </div>

  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/fileinput.js"></script>
  <script type="text/javascript" src="js/base/es.js"></script>
  <script type="text/javascript" src="js/base/datatables.js"></script>
  <script type="text/javascript" src="js/experto/realizarPruebas.js"></script>
</div>
