<div class="col-md-12">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    <div class="" id="EspacioTable">
      <table class="table table-striped" id="tablaVerificarListaEx">
        <thead>
            <th>N°</th>
            <th>Nombre</th>
            <th>Ticked</th>
            <th>Prueba solicitada</th>
            <th>Fecha de solicitud</th>
            <th>Realizar pruebas</th>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <div class="col-md-12" id="formVerifica" style="display: none;">
      <label for="NombreP">Nombre de la solicitud</label>
      <p id="NombreP"></p>
      <label for="DescripcionP">Descripcion del la solicitud</label>
      <p id="DescripcionP"></p>
      <label for="DescripcionP">Ticked de la solicitud</label>
      <p id="tickedP"></p>

      <div class="col-md-12">
        <label for="labelElemetoDoc" class="col-md-12">Elemento de la solicitud</label>
        <p id="labelElemetoDoc" class="col-md-4"></p>
        <a id="ElemetoDocDecargar" href="" class="col-md-8">Descargar</a>
      </div>
      <div class="col-md-12">
          <div class="col-md-6" id="itemsPrueba">
          </div>
          <div class="col-md-6">
            <p id="RequePrueba"></p>
            <select id="RequerimientoSolicitud" class="form-control" multiple>
            </select>
          </div>
      </div>
    </div>


    <div id="SolicitudFalla" class="" style="display: none;">
      <label class="col-md-12">Inconsistencias de la solicitud</label>
      <div class="col-md-6">
          <textarea id="textAreaFlaso" class="form-control"></textarea>
      </div>
    </div>
    <div class="col-md-12" style="display: none;" id="divBtn">
        <button type="button" id="btnEnviar" class="btn btn-info">Enviar</button>
    </div>

    <div id="itemsDocumetosSolicitud" style="display: none;">
      <div class="col-md-12">
        <p id="laRequerimitentos" class="col-md-4"></p>
        <a id="DecargarD" href="" class="col-md-4">Descargar</a>
        <form class="col-md-4">
            <input type="radio" class="radioItem" name="" value="0" id="siOpcionItem">Si
            <input type="radio" class="radioItem" name="" value="1" id="noOpcionItem">No
        </form>
      </div>
    </div>

    <script type="text/javascript" src="js/base/notify.js"></script>
    <script type="text/javascript" src="js/base/datatables.js"></script>
    <script type="text/javascript" src="js/experto/verificar.js"></script>
</div>
