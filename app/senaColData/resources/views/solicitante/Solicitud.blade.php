<div class="col-md-12">
  <form  id="form">
    <article class="col-md-12">
      <div class="col-md-6">
        <label for="nombreSolicitud" class="col-md-12">Nombre de la solicitud</label>
        <div class="col-md-8">
            <input type="text" name="nombreSolicitud" id="nombreSolicitud" class="form-control" placeholder="Ingrese el nombre de la solicitud">
        </div>
      </div>
      <div class="col-md-6">
        <label for="pruebaSolicitud" class="col-md-12">Prueba a solicitar</label>
        <div class="col-md-8">
          <select name="pruebaSolicitud" id="pruebaSolicitud" class="form-control">
            <option value="A0">Selecionar...</option>
          </select>
        </div>
      </div>
    </article>
    <article class="col-md-12">
      <div class="col-md-6">
        <label for="DescripcionSolicitud" class="col-md-12">Descripcion de la solicitud</label>
        <div class="col-md-8">
          <textarea   name="DescripcionSolicitud" id="DescripcionSolicitud" class="form-control" placeholder="Ingrese la descripcion de la solicitud"></textarea>
        </div>
      </div>
      <div class="col-md-6">
        <label for="Documentosolicitud" class="col-md-12">Elemento de la solicitud</label>
        <div class="col-md-12">
          <input type="file" class="form_control FileInput btn" id="Documentosolicitud" name="archivo[]">
        </div>
      </div>
    </article>
    <div id="resultItems" class="col-md-8 col-md-offset-3 ">
    </div>
    <div class="col-md-8 col-md-offset-3">
        <button type="submit" id="btnAgregars" class="btn btn-success">Crear solicitud</button>
    </div>
    <div class=""  style="display: none;">
      {{csrf_field()}}
      <input id="ItemspruebasLabel" name="ItemspruebasLabel" value="">
      <input type="hidden" name="count" id="contador" value="">
    </div>
  </form>

  <div class="col-md-12" id="Itemsprueba" style="display: none;">
    <div class="col-md-12">
      <p  class="col-md-3 Caso" for=""></p>
      <div class="col-md-6">
          <input type="file" class="form_control FileInput btn" id="" name="file">
      </div>
    </div>

  </div>
  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/solicitante/crearSolicitud.js"></script>
</div>
