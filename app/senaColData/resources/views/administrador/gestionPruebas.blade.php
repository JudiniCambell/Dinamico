<div class="col-md-12">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="css/datepicker.css">
  <link rel="stylesheet" type="text/css" href="css/multi-select.css">
  <section>
    <div class="col-md-10">
      <div id="Cpruebas" class="col-md-12">
        <div class="col-md-6">
          <div class="col-md-12">
            <label for="">Crear prueba</label>
            <label for="nombrePrueba" class="col-md-12">Nombre  de la prueba</label>
            <input type="text" id="nombrePrueba" value="" class="form-control">
          </div>
          <div class="col-md-7">
            <label for="descripcionPrueba" class="col-md-12">Descripcion de la prueba</label>
            <textarea id="descripcionPrueba" class="form-control"></textarea>
          </div>
        </div>
        <div class="col-md-6" >
          <div class="col-md-12">
            <label>Añadir requisitos a la prueba</label>
            <label for="nombreItem">Nombre del requisito</label>
            <input type="text" id="nombreItem" value="" class="form-control">
          </div>
          <div class="col-md-6">
            <label for="descripcionitem">Descripcion del requisito</label>
            <textarea id="descripcionitem" class="form-control"></textarea>
          </div>
          <div class="col-md-7">
            <button type="button" id="btnItem" class="btn btn-info">Agregar requisito</button>
          </div>
          <div class="col-md-12" id="tableItemsC" style="display: none;">
            <table id="tableItemsPrueba" class="table-striped">
              <thead>
                <td>N°</td>
                <td>Nombre</td>
                <td>Descricion</td>
                <td></td>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>

        <div class="col-md-6"  style="display: none;" id="divEstado">
          <label for="estado">Estado de la prueba</label>
          <select class="form-control" id="estadoModificado">
            <option value="A0">selecione...</option>
            <option value="0">Habilitada</option>
            <option value="1">Deshabilitada</option>
          </select>
        </div>
        <div class="col-md-6">
          <br>
          <button type="button" id="btnAcciones" class="btn btn-info">Agregar prueba</button>
          <br>
        </div>
      </div>
      <div class="col-md-12" id="divContenidoModificar" style="display: none;">
        <label >Herramientas</label>
        <select  class="herramientaMulti" multiple="multiple" id="MultiSelectHerramienta">

        </select>
        <button type="button" id="btnherramienta" class="btn btn-info">Guardar herramientas</button>
        <div class="col-md-12" id="plantillas">
              <form class="col-md-12" id="formPlantilla">
                {{csrf_field()}}
                <div class="col-md-6">
                  <label for="nombrePlantilla">Nombre de la plantilla</label>
                  <input type="text" name="nombrePlantilla" value="" id="nombrePlantilla" class="form-control">
                </div>
                <div class="col-md-7">
                  <label for="archivo">Archivo plantilla</label>
                  <input type="file" name="archivo[]"  id="archivo" >
                </div>
                <div id="divUrlPlantilla" style="display: none;">
                  <a id="archivoPlantillaM">Descargar</a>
                </div>
                <div class="col-md-6">
                  <label for="">Fecha de vigencia</label>
                  <input type="text" name="fechaVigencia" id="fechaVigencia">
                </div>
                <div class="col-md-7">
                  <input type="hidden" name="data" value='{"estado":"8"}'>
                  <input type="hidden" name="tipoP" id="tipoP" value="0">
                  <input type="hidden" name="prueba" id="prueba" value="">
                  <button type="submit" id="btnGuardarPlantilla" class="btn btn-info">Guardar plantilla</button>
                </div>
              </form>
              <div class="col-md-12">
                <table id="tablePlantillas" class="table-striped">
                  <thead>
                    <th>N°</th>
                    <th>Nombre</th>
                    <th>Formato</th>
                    <th>Fecha de vigencia</th>
                    <th>Estado</th>
                    <!--th>Modificar</th-->
                    <th>Deshabilitar</th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <table id="tableItemsPruebaVista" class="table-striped">
                  <thead>
                    <td>N°</td>
                    <td>Nombre</td>
                    <td>Descricion</td>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
        </div>
      </div>
    </div>
    
  </section>

  <div class="" id="divTable">
    <table id="tablePruebas" class="table table-striped">
      <thead>
        <th>N°</th>
        <th>Nombre</th>
        <th>Descricion</th>
        <th>Estado</th>
        <th>Agregar items</th>
        <th>Modificar prueba</th>
        <th>Deshabilitar</th>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <script type="text/javascript" src="js/base/notify.js"></script>
  <script type="text/javascript" src="js/base/datatables.js"></script>
  <script type="text/javascript" src="js/base/jquery-ui.js"></script>
  <script type="text/javascript" src="js/base/jquery.multi-select.js"></script>
  <script type="text/javascript" src="js/base/jquery.quicksearch.js"></script>
  <script type="text/javascript" src="js/administrador/pruebas.js"></script>
</div>
