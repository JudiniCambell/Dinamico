<?php

namespace App;
use App\Ciudad;
use App\Nombre;
use App\Apellido;
use App\Palabras;
use App\Profesion;
use App\Departamento;
use DB;
use Illuminate\Database\Eloquent\Model;

class Random
{
  var $tab;
  public function consultaDinamica($json){
    $resultSet ="";
    try {
      $tab= $json->tipoDato;
      $i=0;
      $iden=false;
      $tipoDato="";
        switch ($tab) {
          case "1"://Ciudad
            $ca = (int)$json->tamanoCampo;
              $iden='true';
              $nomColumn ="nombreCiudad";
              $tipoDato = DB::table("Territorio")
                ->select("nombreCiudad")
                      ->orderBy(DB::raw('RAND()'))
                      ->take(1)
                      ->get();
                $iden='true1';
                $t=0;
            break;
          case "2"://Departamento
            $ca = (int)$json->tamanoCampo;
            $nomColumn ="nombreDepartamento";
            $iden='true';
              $tipoDato = DB::table("Territorio")
                ->select("nombreDepartamento")
                      ->orderBy(DB::raw('RAND()'))
                      ->take(1)
                      ->get();
                $iden='true1';
                $t=0;
            break;
          case "3"://Departamento/Ciudad
            $tipoDato = DB::table("Territorio")
                ->select("nombreDepartamento","nombreCiudad")
                      ->orderBy(DB::raw('RAND()'))
                      ->take(1)
                      ->get();
            $resultSet= $tipoDato[0]->nombreDepartamento.",".$tipoDato[0]->nombreCiudad;
            break;
          case "4"://Profesion
            $ca = (int)$json->tamanoCampo;
            $nomColumn ="nombreProfesion";
            $iden='true';
              $tipoDato = DB::table("Profesion")
                ->select("nombreProfesion")
                      ->orderBy(DB::raw('RAND()'))
                      ->take(1)
                      ->get();
                $iden='true1';
                $t=0;
            break;
          case "5"://Nombre
            $ca = (int)$json->tamanoCampo;
            $nomColumn ="nombre";
            $iden='true';
              $tipoDato = DB::table("Nombre")
                ->select("nombre")
                      ->orderBy(DB::raw('RAND()'))
                      ->take(1)
                      ->get();
                $iden='true1';
                $t=0;

            break;
          case "6"://Apellido
            $ca = (int)$json->tamanoCampo;
            $nomColumn ="apellido";
            $iden='true';
              $tipoDato = DB::table("Apellido")
                ->select("apellido")
                      ->orderBy(DB::raw('RAND()'))
                      ->take(1)
                      ->get();
                $iden='true1';
                $t=0;
            break;
          case "7"://Cedula
            $resultSet = $this->aleatorioNum(10);
            break;
          case "8"://Telefono celular
             $resultSet = $this->aleatorioCel();
            break;
          case "9"://Telefono fijo
             $resultSet = $this->aleatorioNum(7);
            break;
          case "10"://Contraseña
             $resultSet = $this->aleatorioContrasena($json->tamanoCampo);
            break;
          case "11"://Dirrecion
             $resultSet = $this->aleatorioDirrecion();
            break;
          case "12"://Descripcion
              $resultSet = $this->aleatorioDescripcion($json->tamanoCampo);
              break;
          case "13"://Intervalo
              $resultSet = $this->aleatorioIntervalo($json->arrayInter);
              break;
          case "14"://Varchar
              $ca = (int)$json->tamanoCampo;
              $iden='true';
              $nomColumn ="nomPalabra";
                $tipoDato = DB::table("Palabras")
                	->select("nomPalabra")
                        ->orderBy(DB::raw('RAND()'))
                        ->take(1)
                        ->get();
                  $iden='true1';
                  $t=0;
          break;
          case "15"://int
              if ($json->tipoC==1) {//Con rango
                $resultSet=$this->aletorioNumRango($json->inicioRango,$json->finRango,0);
              }else {//sin rango
                $resultSet = $this->aletorioNum($json->tamanoCampo);
              }
            break;
          case "16"://Decimal de 2
              $resultSet=$this->aletorioNumRango($json->inicioRango,$json->finRango,1);
            break;
          case "17"://Porcentaje
              $resultSet =rand(0,100)."%";
            break;
          case "18"://autoIncrement
              $resultSet  =$json->iniAuto + $json->paso;
              break;
          case "19"://Bolean
                $resultSet = $this->aletorioBolean();
              break;
          case "20"://Fecha Y-M-D
                $tipo = "Y/m/d";
                $fin = $json->fechaFinal;
                $inicio = $json->FechaInicio;
                $resultSet = $this->aletorioDateIntervalo($inicio,$fin,$tipo);
              break;
          case "21"://Fecha M-D-Y
                $tipo = "m/d/Y";
                $fin = $json->fechaFinal;
                $inicio = $json->FechaInicio;
                $resultSet = $this->aletorioDateIntervalo($inicio,$fin,$tipo);
              break;
          case "22"://Fecha D-M-Y
                $tipo = "d/m/Y";
                $fin = $json->fechaFinal;
                $inicio = $json->FechaInicio;
                $resultSet = $this->aletorioDateIntervalo($inicio,$fin,$tipo);
              break;
           case "23"://fecha antes
                $inicio = '1798-01-01';
                $fin = date("Y-m-d");
                $resultSet = $this->aletorioDate($inicio,$fin);
              break;
           case "24"://Fecha despues
                $inicio = date("Y-m-d");
                $fin = '2999-10-31';
                $resultSet = $this->aletorioDate($inicio,$fin);
              break;
          }
          if($iden=='true1') {
            $ca = (int)$json->tamanoCampo;
            $resultSet= $this->aleatorioVarcharRellena($tipoDato,$nomColumn,$ca);
          }
    } catch (Exception $e) {
      echo $e->error;
      $resultSet= "Error".$e->error;
    }
    return $resultSet;
  }

  public  function aletorioCel(){
    $arrOperador =[300,301,304,310,311,312,313,314,320,321,322,315,316,317,318,319,350];
    $cel="";
    for ($i=0; $i <8 ; $i++) {
      if($i==0){
        $tabOp = rand(0,16);
        $cel = $arrOperador[$tabOp];
      }else{
        $cel = $cel . rand(0,9);
      }
    }
    return $cel;
  }
  public function aletorioNum($max){
    $num="";
    for ($i=0; $i < $max ; $i++) {
      $num = $num . rand(0,9);
    }
    return $num;
  }
  public function aletorioDate($datestart,$dateend) {
        $min = strtotime($datestart);
        $max = strtotime($dateend);
        $val = rand($min, $max);
        return date('Y-m-d', $val);
  }
  public function aletorioDateIntervalo($datestart="1798-01-01",$dateend="2999-10-31",$tipo) {
        $min = strtotime($datestart);
        $max = strtotime($dateend);
        $val = rand($min, $max);
        return date($tipo, $val);
  }
  public function aletorioNumRango($inicio,$fin,$tip) {
    $cadenP = "";
    $cadenP = rand($inicio,$fin);
    if ($tip==1) {
      $es=rand(0,99);
      if ($es<9) {
        $es= "0".$es;
      }
      $cadenP=$cadenP.".".$es;
    }
    return $cadenP;
  }
  public function aleatorioContrasena($max){
     $cadena = "ABCDEFGHIJK@LMNOP_Q-RSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
     $longitudCadena=strlen($cadena);
     $res="";
    for ($i=0; $i < $max; $i++) {
       $pos=rand(0,$longitudCadena-1);
       $res .= substr($cadena,$pos,1);
    }
    return $res;
  }
  public function aleatorioDirrecion(){
    $res="";
    $arrCa = ["Carrera","Calle","Transversal","Diagonal","Avenida"];
    $posicion=["Sur","Este","Occidente","Oriente"];
    $letra =["A","A Bis","B","B Bis","C","C Bis","D","D Bis"];
    $bi=["","Bis"];
    $poRe = rand(0,sizeof($posicion)-1);
    $poRe1=rand(0,sizeof($posicion)-1);
    $poSize=sizeof($posicion);
    $res = $this->Randons($arrCa) ." ". rand(1,180)." ".$this->Randons($letra)." # ".rand(1,100)."-".rand(1,80)." ".$this->Randons($bi)." ".$posicion[$poRe];
    return $res;
  }
  public function Randons($arr){
    $rand = rand(0,sizeof($arr)-1);
    return $arr[$rand];
  }
  public function aleatorioDescripcion($max){

    return $res;
  }
  public function aleatorioIntervalo($data){
    $jso = explode(",", $data);
    $pos=rand(0, sizeof($jso)-1);
    $res = $jso[$pos];
    return $res;
  }
  public function aletorioBolean(){
    $bool = rand(0,1);
    $res =true;
      if ($bool==0) {
        $res= false;
      }else if ($bool==1) {
        $res= true;
      }
    return $res;
  }
  public function aleatorioVarcharRellena($dataT,$nomColumn,$ca){
    $tamano =0;
    $tamano = strlen($dataT[0]->$nomColumn);
    if ($ca > $tamano) {
      $res=$dataT[0]->$nomColumn;
      while ($tamano < $ca) {
        $res = $res ."&";
        $tamano++;
      }
    }else if($ca >0){
      $res= substr($dataT[0]->$nomColumn, 0,$ca);
    }else{
      $res= $dataT[0]->$nomColumn;
    }
    return $res;
  }
  function randomArchivos(){
    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz234567890";
    $cad = "";
    for($i=0;$i<10;$i++) {
      $cad .= substr($str,rand(0,120),1);
    }
    return $cad;
  }
  function randomToken($in){
    $cadenP='';
    for ($i=0; $i <$in ; $i++) {
      $cadenP = $cadenP.rand(0,9);
    }
    return $cadenP;
  }
}
