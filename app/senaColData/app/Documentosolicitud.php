<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentosolicitud extends Model
{
  protected $table="Documentosolicitud";
    protected $primaryKey="idCertificadoSolicitud";
    public $timestamps=false;
}
