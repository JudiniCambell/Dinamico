<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estado extends Model
{
  protected $table="Estado";
  protected $primaryKey="idEstado";
  public $timestamps=false;
}
