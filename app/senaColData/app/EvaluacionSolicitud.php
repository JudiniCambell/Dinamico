<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluacionSolicitud extends Model
{
  protected $table="Evaluacionsolicitud";
  protected $primaryKey="idEvaluacionSolicitud";
  public $timestamps=false;
}
