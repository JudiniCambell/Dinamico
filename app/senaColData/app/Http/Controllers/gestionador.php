<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\solicitud;
use App\Documentosolicitud;
use App\itemRequisito;
use Session;

class gestionador extends Controller
{
  function consultarSolicitudesPendientes(){
    try {
      $resultSet = DB::select("call verificarSolicitudes(".Session::get('idCentro').")");
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
  }
  function DocumentosSolicitudes(){
    try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet = DB::select("call requerimientoSolicitud(".$json->prueba.")");
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
  }
  
  function RequerimientoPruebas(){
    try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet = DB::select("call conItemsRequisitos(".$json->option.")");
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
  }
  function selectTestinging(){
    try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet = DB::select("call pruebaSelect(".$json->prueba.")");
    } catch (Exception $e)  {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
  }
  function selectSolicitudTestinging(){
    try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet = DB::select("call SolicitudTestinging(".$json->option.")");
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
  }
  function EnviarSolicitud(){
    $res =$_GET['data'];
    $json = json_decode($res);
    $men="";
    $dad= solicitud::find($json->solicitud);
    if ($json->tiJson=='0') {
      $dad->reporteValoracion=$json->casoError;
      $dad->fkEstado=6;
      $fecha = date('Y-m-j');
      $nuevafecha = strtotime ( '+7 day' , strtotime ($fecha) ) ;
      $dad->fechaFinalizacion=$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
      $can=" enviaron ";
    }else if($json->tiJson=='1'){
      $dad->fkTestig=$json->testingSelec;
      $dad->fkEstado=2;
      $can=" asigno el tester.";
    }
    if ($dad->save()) {
      if ($json->tiJson=='0') {
        $Rc=0;
        $itemFail = explode(",", $json->itemsFalla);
        for ($i=0; $i < count($itemFail) ; $i++) {
          $itemSolicitud= Documentosolicitud::find($itemFail[$i]);
          $itemSolicitud->estadoDocumento =1;
          if ($itemSolicitud->save()) {
              $Rc++;
          }
        }
        if ($Rc==count($itemFail)) {
          $can=$can." las correciones.";
        }
      }
      $men="true$%se ".$can;
    }else{
      $men="false$%no se ".$can;
    }
    return $men;
  }
}
