<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\EvaluacionSolicitud;
use App\Documentosolicitud;
use App\plantillasReportes;
use App\solicitud;
use Session;
use App\Random;

class testing extends Controller{

  function solicitudes(){
    try {
      if (Session::get('idTUsuario')==3) {
        $resultSet = DB::select("call solicitudesTesting(".Session::get('idUser').",2)");
      }
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
   return response()->json($resultSet);
  }

  function EnviarSolicitud(){
    $res =$_GET['data'];
    $json = json_decode($res);
    $men="";
    $dad= solicitud::find($json->solicitud);
    if ($json->tiJson=='0') {
      $dad->reporteValoracion=$json->casoError;
      $dad->fkEstado=8;
      $fecha = date('Y-m-j');
      $nuevafecha = strtotime ( '+7 day' , strtotime ($fecha) ) ;
      $dad->fechaFinalizacion=$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
      $can=" enviaron";
    }else if($json->tiJson=='1'){
      $dad->fkEstado=3;
      $can=" se valido la solicitud";
    }
    if ($dad->save()) {
      if ($json->tiJson=='0') {
        $Rc=0;
        $itemFail = explode(",", $json->itemsFalla);
        for ($i=0; $i < count($itemFail) ; $i++) {
          $itemSolicitud= Documentosolicitud::find($itemFail[$i]);
          $itemSolicitud->estadoDocumento =1;
          if ($itemSolicitud->save()) {
              $Rc++;
          }
        }
        if ($Rc==count($itemFail)) {
          $can=$can." las correciones.";
        }
      }
      return $men="true$%se ".$can;
    }else{
      return $men="false$%no se ".$can;
    }
  }
  function SolicitudesPruebas(){
    try {
      if (Session::get('idTUsuario')==3) {
        $resultSet = DB::select("call solicitudesTesting(".Session::get('idUser').",3)");
        return $resultSet;
      }
     } catch (Exception $e) {
      echo $e->getMessage();
      return $e->getMessage();
     }
  }
  function Itemsprueba(){
    try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet= DB::select("call  conItemsRequisitos(".$json->prueba.")");
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
  }
function PlantillasReportes(){
  try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet = plantillasReportes::Select('idPlantilla','nombrePlantilla')
                ->where("fkPrueba2",$json->prueba)
                ->get();
          return response()->json($resultSet);
    } catch (Exception $e) {
      echo $e->getMessage();
     return  $resultSet= $e->getMessage();
    }

 }
function Herramienta(){
  try {
    $res =$_GET['data'];
    $json = json_decode($res);
    $resultSet = DB::select("call herramientasUrl(".$json->prueba.")");
  } catch (Exception $e) {
    echo $e->getMessage();
    $resultSet= $e->getMessage();
  }
  return response()->json($resultSet);
}
function linkPlantillas(){
  try {
      $res =$_GET['data'];
      $json = json_decode($res);
      $resultSet = plantillasReportes::Select('idPlantilla','formatoPlantilla','descripcionPlantilla')
                ->where("idPlantilla",$json->plantilla)
                ->get();
    } catch (Exception $e) {
      echo $e->getMessage();
      $resultSet= $e->getMessage();
    }
    return response()->json($resultSet);
}
function parciales(){
  try {
    $dates=date('y/m/d', time());
    $arrUrl=[];
    $plantilla = $_POST['arrayPlantilla'];
    $arrArchivos  = explode(",", $plantilla);
      if (isset($_FILES['archivo'])) {
        $dateF=str_replace('/','_',$dates);
        foreach ($_FILES['archivo']['name'] as $indice => $archivo) {
          if (!empty($archivo)) {
            $tipoDato =new Random();
            $nomAleatoria = $tipoDato->randomArchivos(10);
            $nomArchivoD = $nomAleatoria.$dateF.Session::get('idUser');
            move_uploaded_file(
              $_FILES['archivo']['tmp_name'][$indice],
              'Archivos/defecto/'.$archivo
            );
            $ext = pathinfo($archivo, PATHINFO_EXTENSION);
            rename("Archivos/defecto/".$archivo, "Archivos/solicitudes/documentosTesting/".$nomArchivoD.'.'.$ext);
            $nomArchivoF=$nomArchivoD.'.'.$ext;
            array_push($arrUrl,$nomArchivoF);
          }
        }
      }
      $cc=0;
      for ($i=0; $i <count($arrArchivos); $i++) {
         $item = new Documentosolicitud;
         $item->fktipoDocument=2;
         $item->fkSolicitud= $_POST['solicitud'];
         $item->fechaDocumento=$dates;
         $item->estadoDocumento=0;
         $item->descripcionDocumento=$arrArchivos[$i];
         $item->urlArchivo= $arrUrl[$i];
         if ($item->save()) {
           $cc++;
         }else{
           $cc=$cc-1;
         }
       }
       $s= count($arrArchivos);
       if ($s == $cc) {
           return $resultSet="true$%Se a enviado los resultados parciales";
       }else{
           return $resultSet= ("false$%No se a enviado los resultados parciales");
       }
    } catch (Exception $e) {
      echo $e->getMessage();
      return $resultSet= $e->getMessage();
    }
 }

function peticionfinal(){
  try {
    $numItems = $_POST['numItems'];
    $cc=0;
    $dates=date('y/m/d', time());
    $arrUrl=[];
      if (isset($_FILES['archivo'])) {
        $dateF=str_replace('/','_',$dates);
        foreach ($_FILES['archivo']['name'] as $indice => $archivo) {
          if (!empty($archivo)) {
            $tipoDato =new Random();
            $nomAleatoria = $tipoDato->randomArchivos(10);
            $nomArchivoD = $nomAleatoria.$dateF.Session::get('idUser').date('y',time()).date('m', time()).date('d', time()).date('his', time());
            move_uploaded_file(
              $_FILES['archivo']['tmp_name'][$indice],
              'Archivos/defecto/'.$archivo
            );
            $ext = pathinfo($archivo, PATHINFO_EXTENSION);
            rename("Archivos/defecto/".$archivo, "Archivos/solicitudes/documentosFinales/".$nomArchivoD.'.'.$ext);
            $nomArchivoF=$nomArchivoD.'.'.$ext;
            array_push($arrUrl,$nomArchivoF);
          }
        }
      }
      $cc=0;
      $ccountE=0;
      for ($q=0; $q < count($arrUrl); $q++) {
        $item = new Documentosolicitud;
        $item->fktipoDocument=5;
        $item->fkSolicitud= $_POST['solicitud'];
        $item->fechaDocumento=$dates;
        $item->descripcionDocumento="Reporte final de la prueba";
        $item->urlArchivo=$arrUrl[$q];
        $item->estadoDocumento =0;
        if ($item->save()) {
          $cc++;
        }else{
          $cc=$cc-1;
        }
      }
      $valoracionLetra="";
      for ($i=0; $i <$numItems ; $i++) {
        $eva= new EvaluacionSolicitud;
        $eva->idEvaluacionSolicitud = 0;
        if ($_POST['check'.$i]=='1') {
          $ccountE++;
          $valoracionLetra="Aprobado";
        }else {
          $valoracionLetra="Reprobado";
        }
        $eva->valoracion   = $valoracionLetra;
        $eva->observacion  = $_POST['textA'.$i];
        $eva->fkSolicitud  = $_POST['solicitud'];
        $eva->fkItemPrueba = $_POST['item'.$i];
        if ($eva->save()) {
          $cc++;
        }else{
          $cc=$cc-1;
        }
      }
      $dad= solicitud::find($_POST['solicitud']);
      if ($numItems==$ccountE) {
        $dad->fkEstado=4;
        $estado="aprobado";
      }else{
        $dad->fkEstado=7;
        $estado="reprobado";
      }
      $dad->fechaFinalizacion=date('y/m/d', time());
      if ($dad->save()) {
        $cc++;
      }else{
        $cc=$cc-1;
      }
      $guardaTo= count($arrUrl)+$numItems+1;
      if ($guardaTo == $cc) {
          $resultSet="true$%Se a ".$estado." la solicitud";
      }else{
          $resultSet= ("false$%No se a calificado la solicitud");
      }
     return$resultSet;
    } catch (Exception $e) {
      echo $e->getMessage();
      return $e->getMessage();
    }
  }
}
