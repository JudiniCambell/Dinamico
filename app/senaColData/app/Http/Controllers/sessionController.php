<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\usuario;
use App\token;
use Session;
use Hash;

class sessionController extends Controller{
  public function getIntro(){
      return view('incio/index');
  }
  public function camEstadoUsuario(){
    $res = $_GET['data'];
    $json = json_decode($res);
    $resultSet = token::select("idToken","numeroToken","documento")
                ->where("numeroToken",$json->token)
                ->where("documento",$json->usuario)
                ->get();
      if (count($resultSet)>0) {
          foreach ($resultSet as $key) {
            try {
              $passHash = password_hash($json->con, PASSWORD_BCRYPT);
              $cambio =  DB::select("call cambiarEstadoUsuario('".$json->usuario."','".$passHash."')");
              $delete= DB::select("Delete from tokenUser where idToken=".$key->idToken."");
              return ("True$%se le a asignado la contraseña al usuario ".$json->usuario);
            } catch (Exception $e) {
               return ("false$%No se le a asignado la contraseña al usuario ".$json->usuario);
            }
          }
      }else{
        return ("false$%No se le a asignado la contraseña al usuario ".$json->nombre." ".$json->apellido);
      }
  }

  public function cambiarContrasena(){
    try {
      $resultSet = token::select("numeroToken","documento")
                  ->where("numeroToken",$_GET['op'])
                  ->get();
        if (count($resultSet)>0) {
          foreach ($resultSet as $key) {
            if ($key->documento==$_GET['usu']) {
              $data=["token"=>$key->numeroToken ,"usu"=>$key->documento];
              return view('incio/cambioContrasena',$data);
            }else{
              return view('incio/index');
            }
          }
        }else {
          return view('incio/index');
        }
    } catch (Exception $e) {
      return view('incio/index');
    }
  }

  public function postIntro(){
    try {
      if ($_POST != null ) {
        $bandera =0;
        $tip=0;
        $array=[];
        $tokens= $_POST['pass'];
        $user = db::select("select password from Usuario where documento= '".$_POST['usu']."' ");
        foreach ($user as $key) {
          $tokenBd=$key->password;
        }
        if (password_verify($_POST['pass'],$tokenBd)) {
          $resultSet = DB::select("call  sessionC('".$_POST['usu']."')");
          foreach ($resultSet as $key ) {
              Session::put('Nombre',$key->nombres." ".$key->apellidos);
              Session::put('idTUsuario',$key->idRol);
              Session::put('idUser',$key->idUsuario);
              Session::put('idCentro',$key->fkCentroFormacion);
              $tip =$key->idRol;
          }
       switch ($tip) {
            case 0:
              return view('incio/index');
                break;
            case 1:
              return view('solicitante/pricipalSolicitante');
                break;
            case 2:
               return view('gestionador/pricipalGestionador');
                break;
            case 3:
                return view('experto/pricipalExperto');
                 break;
            case 4:
                return view('administrador/pricipalAdministrador');
               break;
            case 5:
                return view('generador/crearArchivos');
               break;
            default:
                  return view('incio/index');
              break;
            }
            }else{
              return view('incio/index');
            }
          }else{
            Session::forget('idTUsuario');
            Session::forget('idUser');
            Session::forget('idCentro');
            return view('incio/index');
          }
        } catch (Exception $e) {
            Session::forget('idTUsuario');
            Session::forget('idUser');
            Session::forget('idCentro');
        }
      }
}
