<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\tipoDato;
use App\Ciudad;
use App\Departamento;
use DB;
class crud_Controller extends Controller
{
  function selectTipDato (){
  try {
    $tipoDato = tipoDato::all();
  } catch (Exception $e) {
    $tipoDato= $e->getMessage();
  }
  return response()->json($tipoDato);
}
function autoComplet(){
  try {
    $tipoDato[0] = array("colum"=>"Ciudad");
    $tipoDato[1] = array("colum"=>"Departamento");
    $tipoDato[2] = array("colum"=>"Profesion");
    $tipoDato[3] = array("colum"=>"Primer Nombre");
    $tipoDato[4] = array("colum"=>"Segundo Nombre");
    $tipoDato[5] = array("colum"=>"Primer apellido");
    $tipoDato[6] = array("colum"=>"Segundo apellido");
    $tipoDato[7] = array("colum"=>"Cedula");
    $tipoDato[8] = array("colum"=>"Telefono celular");
    $tipoDato[9] = array("colum"=>"Telefono fijo");
    $tipoDato[10] = array("colum"=>"Contraseña");
    $tipoDato[11] = array("colum"=>"Direcion");
    $tipoDato[12] = array("colum"=>"Estado civil");
    $tipoDato[13] = array("colum"=>"Tipo de sangre");

  } catch (Exception $e) {
    $tipoDato= $e->getMessage();
  }
  return json_encode($tipoDato);
}
function autoDepartamento(){
  try {
    $tipoDato = Departamento::all();
  } catch (Exception $e) {
    $tipoDato= $e->getMessage();
  }
  return response()->json($tipoDato);
}
function autoCity(){
  try {
    $res =$_GET['data'];
    $tipoDato = Ciudad::where('fkIdDepartamento', $res)
            ->get();
  } catch (Exception $e) {
    $tipoDato= $e->getMessage();
  }
  return response()->json($tipoDato);
}
}
