<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoDocumento extends Model
{
  protected $table="TipoDocumento";
  protected $primaryKey="idTipoDocumento";
  public $timestamps=false;
}
