<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoUsuario extends Model
{
  protected $table="Tipousuario";
  protected $primaryKey="idTipousuario";
  public $timestamps=false;
}
