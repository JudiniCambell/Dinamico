<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipoPrueba extends Model
{
  protected $table="Tipoprueba";
  protected $primaryKey="idTipoPrueba";
  public $timestamps=false;
}
