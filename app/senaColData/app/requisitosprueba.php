<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requisitosprueba extends Model
{
  protected $table="requisitosprueba";
  protected $primaryKey="idItem";
  public $timestamps=false;
}
