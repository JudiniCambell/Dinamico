$(document).ready(function(){
  var obsession = {"tipoUsuario":"14"};
  var selector = [], hilo = [], jso = [], data = [],datos=[],datJson="";
  var ob = new $.Luna("principalAdministrador");
  var Base = ob.Vivo("principalAdministrador");
  datos[0]={opcion:'0',icon:"male user",label:"#stroked-male-user",titulo:"Gestion de usuarios"};
  datJson='{"option":"'+0+'","tip":"'+obsession.tipoUsuario+'"}';
  jso[1] = {
    url: '../../linkAdmi',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: datJson
  };
  ajax(1);
  $(document).on('click','.labelS',function(e){
    var s = this.id;
    $("#btnIngresar").prop('disabled', true);
    switch (s) {
      case '0':
        datos[0]={opcion:s,icon:"male user",label:"#stroked-male-user",titulo:"Gestion de usuarios"};
       break;
      case '1':
        datos[0]= {opcion:s,icon:"table",label:"#stroked-table",titulo:"Gestion de pruebas"};
       break;
      case '2':
        datos[0]= {opcion:s,icon:"table",label:"#stroked-table",titulo:"Gestion de herramientas"};
       break;
      case '3':
        datos[0]={opcion:s};
       break;
    }
    datJson='{"option":"'+s+'","tip":"'+obsession.tipoUsuario+'"}';
    jso[1] = {
      url: '../../linkAdmi',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          hilo[i].terminate();
          peticionCompleta(i);
      };
  };

  function peticionCompleta(i) {
      if(i==1){
        if (datos[0].opcion=='3') {
          $("#formComplet").html("");
          $("#formComplet").html(data[i]);
        }else{
          $("#contenido").html("");
          $("#contenido").html(data[i]);
          $("#useClassModulo").attr('class','');
          $("#useClassModulo").attr('class','glyph stroked '+datos[0].icon+'');
          $("#useClass1Modulo").attr('xlink:href',datos[0].label);
          $("#titulo").text(datos[0].titulo);
        }
      }
    }
});
