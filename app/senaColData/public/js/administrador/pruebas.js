$(document).ready(function(){
  var selector = [], hilo = [], jso = [], data = [],datos=[],prueba=0,arrItemsPrueba=[],arrItemsHerrramienta=[],arrItemsHerrramientaPrueba=[],arraySelecionHerramienta=[],arrayEliminarHerramienta=[];
  var ob = new $.Luna("prueba");
  ob.Vivo("pruebas");
  urlrelativo=ob.Relativo();
  selector[0]= $("#tablePruebas");
  ob.TablaEspa(selector[0]);
  datos[0] = {nombre:"PruebasConsulta"};
  datJson = '{"estado":"0"}';
  jso[0] = {
    url:'../../pruebas',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: datJson
  };
  ajax(0);

  $('.herramientaMulti').multiSelect({
          selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Busca una herrmienta...'>",
          selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Busca una herrmienta...'>",
          afterInit: function (ms) {
              var that = this,
                      $selectableSearch = that.$selectableUl.prev(),
                      $selectionSearch = that.$selectionUl.prev(),
                      selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                      selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                      .on('keydown', function (e) {
                          if (e.which === 40) {
                              that.$selectableUl.focus();
                              return false;
                          }
                      });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                      .on('keydown', function (e) {
                          if (e.which == 40) {
                              that.$selectionUl.focus();
                              return false;
                          }
                      });
          },
          afterSelect: function (val) {
              this.qs1.cache();
              arraySelecionHerramienta.push(val);
              var busqueda = $.inArray(val, arrayEliminarHerramienta);
              arrayEliminarHerramienta.splice(busqueda, 1);
              this.qs2.cache();
          },
          afterDeselect: function (val) {
              this.qs1.cache();
              this.qs2.cache();
              arrayEliminarHerramienta.push(val);
              var busqueda = $.inArray(val, arraySelecionHerramienta);
              arraySelecionHerramienta.splice(busqueda, 1);
          }
      });

  $(document).on('click','.btnAgregarItems',function(e){
    prueba = this.id;
    $("#Cpruebas").hide();
    $("#divTable").hide();
    $("#divContenidoModificar").show();
    selector[2]=$("#tableItemsPruebaVista");
    ob.setCons("requerimiento");
    ob.TablaEspa(selector[2]);
    datos[2] = {nombre:"itemsPrueba"};
    $("#prueba").val(prueba);
    datJson = '{"estado":"5","prueba":"'+prueba+'"}';
    jso[2] = {
      url:'../../pruebas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(2);
  });

  $("#btnherramienta").click(function(){
    datos[1] = {nombre:"btn"};
    datJson = '{"estado":"10","pruebaSelect":"'+prueba+'","Items":"'+arraySelecionHerramienta+'","arrE":"'+arrayEliminarHerramienta+'"}';
    jso[1] = {
      url:'../../pruebas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  $(document).on('click','.btnModificar',function(e){
    $("#divEstado").show();
    array = this.id.split("$$");
    prueba = array[0];
    $("#fechaVigencia").datepicker({
          minDate:'0',
          dateFormat: 'dd/mm/yy',
          changeMonth: true,
          changeYear: true
    });
    $("#nombrePrueba").val(array[1]);
    $("#descripcionPrueba").val(array[2]);
        $("#estadoModificado").val(array[3]).change();
    $('html, section').animate({scrollTop:0}, 'slow');
    $("#btnAccion").text("Modificar prueba");
  });

  $(document).on('click','.btnDeshabilitar',function(e){
    datos[1] = {nombre:"btn",tipoConsulta:"0"};
    datJson = '{"estado":"3","prueba":"'+this.id+'"}';
    jso[1] = {
      url:'../../pruebas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  $(document).on('click','.btnModificarPlantilla',function(e){
    array = this.id.split("$$");
    $("#prueba").val(array[0]);
    $("#nombrePlantilla").val(array[1]);
    $("#fechaVigencia").val(array[2]);
    $("#archivoPlantillaM").attr(array[3]);
    $("#tipoP").val("1");
    $("#btnGuardarPlantilla").text("Modificar plantilla");
    $("#divUrlPlantilla").show();
  });

  $(document).on('click','.btnDeshabilitarPlantilla',function(e){
    datos[1] = {nombre:"btn",tipoConsulta:"2"};
    datJson = '{"estado":"9","plantilla":"'+this.id+'"}';
    jso[1] = {
      url:'../../pruebas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  $("#btnAcciones").click(function(){
    if ($("#btnAccion").text() =="Modificar prueba") {
        datos[1] = {nombre:"btn",tipoConsulta:"0"};
        datJson = '{"estadoModificado":"'+$("#estadoModificado").val()+'","prueba":"'+prueba+'","estado":"2","nombre":"'+$("#nombrePrueba").val()+'","descripcion":"'+$("#descripcionPrueba").val()+'"}';
    }else{
      var arrItemsPruebaS="";
       for (var i = 0; i < arrItemsPrueba.length; i++) {
         arrItemsPruebaS= arrItemsPruebaS+"$$"+arrItemsPrueba[i].nombre+","+arrItemsPrueba[i].descripcion+"";
       }
        datos[1] = {nombre:"btn",tipoConsulta:"0"};
        datJson = '{"estado":"1","nombre":"'+$("#nombrePrueba").val()+'","descripcion":"'+$("#descripcionPrueba").val()+'","Items":"'+arrItemsPruebaS+'"}';
    }
    jso[1] = {
      url:'../../pruebas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    $("#estado").val("");
    $("#divEstado").hide();
    $("#btnAccion").text("Agregar prueba");
    ajax(1);
  });

  var contItemsS =0;
  $("#btnItem").click(function(){
    if (contItemsS==0) {
      $("#tableItemsC").show();
      ob.setCons('requisitos de la prueba');
      ob.TablaEspa($("#tableItemsPrueba"));
    }
    //itemsPruebaAñadir
    datos[6] = {nombre:"itemsPruebaAñadir"};
    arrItemsPrueba.push({'idP':''+contItemsS+'','nombre':''+$("#nombreItem").val()+'','descripcion':''+$("#descripcionitem").val()+''});
    contItemsS++;
    $("#nombreItem").val("");
    $("#descripcionitem").val("");
    ob.cargarTabla(arrItemsPrueba,$("#tableItemsPrueba"),datos[6]);
  });

  $(document).on('click','.btnEliminarItems',function(e){
    var busqueda = $.inArray(this.id, arrItemsPrueba);
    arrItemsPrueba.splice(busqueda, 1);
    ob.cargarTabla(arrItemsPrueba,$("#tableItemsPrueba"),datos[6]);
  });

  $("#formPlantilla").submit(function(){
    $.ajax({
      url:urlrelativo+'/pruebas',
      type:'POST',
      data: new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      success:function(resultSet){
        resultAjax(resultSet);
      },
      error:function(){
        alert("Error fatal comuniquese con el administrador del sistema");
      }
    });
    return false;
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          ajaxResult(i);
          hilo[i].terminate();
      };
  };

  function  resultAjax(data){
    var daMen = data.split("$%");
    var men = "";
    if (daMen[0] == "true") {
        estado = ("success");
        men = daMen[1];
    } else {
        estado = ("error");
        men = daMen[1];
    }
    $.notify(men, estado);
    selector[3]=$("#tablePlantillas");
    ob.setCons("plantillas");
    ob.TablaEspa(selector[3]);
    datos[3] = {nombre:"plantillasPrueba"};
    datJson = '{"estado":"7","prueba":"'+prueba+'"}';
    jso[3] = {
      url:'../../pruebas',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    $("#nombrePlantilla").val("");
    $("#archivo").val("");
    $("#fechaVigencia ").val("");
    ajax(3);
  }

  function ajaxResult(i){
    if(i==1){
      var daMen = data[i].split("$%");
      var men = "";
      if (daMen[0] == "true") {
          estado = ("success");
          men = daMen[1];
      } else {
          estado = ("error");
          men = daMen[1];
      }
      $.notify(men, estado);
      datos[22]= {opcion:1,icon:"table",label:"#stroked-table",titulo:"Gestion de pruebas"};
      datJson='{"option":"1","tip":"14"}';
      jso[22] = {
        url: '../../linkAdmi',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(22);
      switch (datos[i].tipoConsulta) {
        case '0':
            datos[0] = {nombre:"PruebasConsulta"};
            datJson = '{"estado":"0"}';
            jso[0] = {
              url:'../../pruebas',
              token: $('meta[name="csrf-token"]').attr('content'),
              dat: datJson
            };
            ajax(0);
            break;
        case '1':
            datos[2] = {nombre:"itemsPrueba"};
            datJson = '{"estado":"5","prueba":"'+prueba+'"}';
            jso[2] = {
              url:'../../pruebas',
              token: $('meta[name="csrf-token"]').attr('content'),
              dat: datJson
            };
            ajax(2);
          break;
        case '2':
          selector[3]=$("#tablePlantillas");
          ob.setCons("plantillas");
          ob.TablaEspa(selector[3]);
          datos[3] = {nombre:"plantillasPrueba"};
          datJson = '{"estado":"7","prueba":"'+prueba+'"}';
          jso[3] = {
            url:'../../pruebas',
            token: $('meta[name="csrf-token"]').attr('content'),
            dat: datJson
          };
          ajax(3);
          break;
      }
    }else if (i==2) {
      selector[3]=$("#tablePlantillas");
      ob.setCons("plantillas");
      ob.TablaEspa(selector[3]);
      datos[3] = {nombre:"plantillasPrueba"};
      datJson = '{"estado":"7","prueba":"'+prueba+'"}';
      jso[3] = {
        url:'../../pruebas',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
    ajax(3);
    }else if (i==3) {
      datos[4] = {nombre:"btn"};
      datJson = '{"estado":"11","prueba":"'+prueba+'"}';
      jso[4] = {
        url:'../../pruebas',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(4);
    }else if (i==4) {
      var jsoItems = jQuery.parseJSON(data[i]);
      for (var i = 0; i < jsoItems.length; i++) {
        arrItemsHerrramienta.push({id:jsoItems[i].idHerramienta,nombre:jsoItems[i].nombre});
        arrItemsHerrramientaPrueba.push(jsoItems[i].idHerramienta);
      }
      if (arrItemsHerrramientaPrueba.length==0) {
        arrItemsHerrramientaPrueba=false;
      }
      datos[5] = {nombre:"btn"};
      datJson = '{"estado":"12","items":"'+arrItemsHerrramientaPrueba+'"}';
      jso[5] = {
        url:'../../pruebas',
        token: $('meta[name="csrf-token"]').attr('content'),
        dat: datJson
      };
      ajax(5);
    }else if (i==5) {
      var jsoItems = jQuery.parseJSON(data[i]);
      for (var i = 0; i < jsoItems.length; i++) {
        arrItemsHerrramienta.push({id:jsoItems[i].herramienta,nombre:jsoItems[i].nombre ,tipo:false});
      }
      datos[5]={nombre:"MultiSelect",compuesto:true};
      ob.cargarTabla(arrItemsHerrramienta,$("#MultiSelectHerramienta"),datos[5]);
    }else if (i==22){
      $("#contenido").html("");
      $("#contenido").html(data[i]);
      $("#useClassModulo").attr('class','');
      $("#useClassModulo").attr('class','glyph stroked '+datos[22].icon+'');
      $("#useClass1Modulo").attr('xlink:href',datos[22].label);
      $("#titulo").text(datos[22].titulo);
    }
  }
});
