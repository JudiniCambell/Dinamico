$(document).ready(function(){
  var selector = [], hilo = [], jso = [], data = [],datos=[],prueba=0,tes=false,arraySelecionPrueba=[],arrayEliminarPrueba=[],arrUsuarioJ=[],ciudad=0,centroFormacion=0,usuario=0,arrTipoUsuarioE="",Usutesting=0;
  var ob = new $.Luna("usuarios");
  ob.Vivo("usuario");
  selector[10]= $("#TableUser");
  ob.TablaEspa(selector[10]);
  datos[10] = {nombre:"consultaUsuarios"};
  datJson = '{"estado":"0"}';
  jso[10] = {
    url:'../../usuarios',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: datJson
  };
  ajax(10);

  $('.itemSelect').multiSelect({
          selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Busca una prueba...'>",
          selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Busca una prueba...'>",
          afterInit: function (ms) {
              var that = this,
                      $selectableSearch = that.$selectableUl.prev(),
                      $selectionSearch = that.$selectionUl.prev(),
                      selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                      selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';
              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                      .on('keydown', function (e) {
                          if (e.which === 40) {
                              that.$selectableUl.focus();
                              return false;
                          }
                      });
              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                      .on('keydown', function (e) {
                          if (e.which == 40) {
                              that.$selectionUl.focus();
                              return false;
                          }
                      });
          },
          afterSelect: function (val) {
              this.qs1.cache();
              arraySelecionPrueba.push(val);
              var busqueda = $.inArray(val, arrayEliminarPrueba);
              arrayEliminarPrueba.splice(busqueda, 1);
              this.qs2.cache();

          },
          afterDeselect: function (val) {
              this.qs1.cache();
              this.qs2.cache();
              arrayEliminarPrueba.push(val);
              var busqueda = $.inArray(val,arraySelecionPrueba);
              arraySelecionPrueba.splice(busqueda, 1);
          }
      });
    $(document).on('click','.btnDeshabilitarUsuario',function(e){
     datos[5] = {nombre:"btn"};
     datJson = '{"estado":"8","user":"'+this.id+'"}';
     jso[5] = {
       url:'../../usuarios',
       token: $('meta[name="csrf-token"]').attr('content'),
       dat: datJson
     };
    ajax(5);
   });

  $(document).on('click','.btnModificarUsuario',function(e){
    $("#divDocumento").hide();
    var arra = this.id.split("$$");
    $('html, section').animate({scrollTop:0}, 'slow');
    usuario = arra[0];
    $("#nombre").val(arra[1]);
    $("#apellidos").val(arra[2]);
    $("#correo").val(arra[3]);
    $("#departamento option[value="+ arra[4] +"]").attr("selected",true);
    $("#btnguardar").text("Modificar usuario");
    $("#opcionesTesting").hide();
    ciudadfun(arra[4],1,arra[5],arra[6]);
  });

 $("#btnguardar").click(function(){
   var arrUsuario="";
   for (var i = 0; i < arrUsuarioJ.length; i++) {
     if (i==(arrUsuarioJ.length-1)) {
       arrUsuario=arrUsuario+arrUsuarioJ[i];
     }else{
       arrUsuario=arrUsuario+arrUsuarioJ[i]+",";
     }
   }
   if ($("#btnguardar").text()=="Modificar usuario") {
     datJson = '{"estado":"7","usuario":"'+usuario+'","centroFormacion":"'+$("#centroFormacion").val()+'","nombre":"'+$("#nombre").val()+'","apellido":"'+$("#apellidos").val()+'","correo":"'+$("#correo").val()+'","TUsuario":"'+$("#selectTipoUsuario").val()+'","arrPrueba":"'+arraySelecionPrueba+'","arrayEliminarPrueba":"'+arrayEliminarPrueba+'"}';
   }else{
     datJson = '{"estado":"6","centroFormacion":"'+$("#centroFormacion").val()+'","documento":"'+ $("#documento").val()+'","nombre":"'+$("#nombre").val()+'","apellido":"'+$("#apellidos").val()+'","correo":"'+$("#correo").val()+'","TUsuario":"'+$("#selectTipoUsuario").val()+'","arrPrueba":"'+arraySelecionPrueba+'"}';
   }
   jso[5] = {
     url:'../../usuarios',
     token: $('meta[name="csrf-token"]').attr('content'),
     dat: datJson
   };
   datos[5] = {nombre:"btn"};
   $("#btnguardar").text("Guardar usuario");
   ajax(5);
 });

count=0;

$("#selectTipoUsuario").change(function(){
  var opt =$("#selectTipoUsuario").val();
  if (opt==3) {
    $("#opcionesTesting").show();
  }
});

/*$("#btnAgregarTipo").click(function(){
  var opt =$("#selectTipoUsuario").val();
  if (opt==3) {
    $("#opcionesTesting").show();
  }
  arrUsuarioJ.push(opt);
  $('#selectTipoUsuario option[value="'+opt+'"]').attr("disabled", true);
  var clon = $("#clonTipoUsuario").clone();
  clon.find(".rowElement").attr('id','rowElement'+count);
  clon.find(".labelTipoUsuario").text($("#selectTipoUsuario  option:selected").text());
  clon.find(".btnELiminarTipo").attr('value','rowElement'+count+"$$"+$("#selectTipoUsuario").val());
  clon.children().appendTo($("#contenedorClon"));
  count++;
});*/

 $(document).on('click','.btnELiminarTipo',function(e){
   var arr = $(this).val().split("$$");
   if (arr[1]==3) {
     Usutesting=0;
     $("#opcionesTesting").hide();
   }
   if (i==(arrTipoUsuarioE.length-1)) {
     arrTipoUsuarioE=arrTipoUsuarioE+arr[1];
   }else{
     arrTipoUsuarioE=arrTipoUsuarioE+arr[1]+",";
   }
   $("#selectTipoUsuario option[value=" + arr[1]+ "]").removeAttr('disabled');
   var busqueda = $.inArray(arr[1],arrUsuarioJ);
   arrUsuarioJ.splice(busqueda, 1);
   $("#"+arr[0]).remove();
 });

  $("#departamento").change(function(){
    if ($(this).val()!="A0") {
      ciudadfun($(this).val());
    }
    $('#Ciudad option[value="A0"]').attr("selected", "selected");
    $('#centroFormacion option[value="A0"]').attr("selected", "selected");
  });

  $("#Ciudad").change(function(){
    if ($(this).val()!="A0") {
      centroFormacionfun($(this).val());
    }
    $('#centroFormacion option[value="A0"]').attr("selected", "selected");
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          resultAjax(i);
          hilo[i].terminate();
      };
  };

  function  resultAjax(i){
    switch (i) {
      case 10:
        datos[1] = {nombre:"Select",opt:"limpiaN"};
        selector[1]=$("#departamento");
        datJson = '{"estado":"1"}';
        jso[1] = {
          url:'../../usuarios',
          token: $('meta[name="csrf-token"]').attr('content'),
          dat: datJson
        };
        ajax(1);
        break;
     case 1:
         datos[2] = {nombre:"Select",opt:"limpiaN"};
         selector[2]=$("#selectTipoUsuario");
         datJson = '{"estado":"4"}';
         jso[2] = {
           url:'../../usuarios',
           token: $('meta[name="csrf-token"]').attr('content'),
           dat: datJson
         };
         ajax(2);
        break;
      case 2:
          datos[3] = {nombre:"MultiSelect"};
          selector[3]=$("#SelectIpruebas");
          datJson = '{"estado":"5"}';
          jso[3] = {
            url:'../../usuarios',
            token: $('meta[name="csrf-token"]').attr('content'),
            dat: datJson
          };
          ajax(3);
        break;
      case 5:
            var daMen = data[i].split("$%");
            var men = "";
            if (daMen[0] == "true") {
                estado = ("success");
                men = daMen[1];
            } else {
                estado = ("error");
                men = daMen[1];
            }
            $.notify(men, estado);
            datos[22]={opcion:0,icon:"male user",label:"#stroked-male-user",titulo:"Gestion de usuarios"};
            datJson='{"option":"0","tip":"14"}';
            jso[22] = {
              url: '../../linkAdmi',
              token: $('meta[name="csrf-token"]').attr('content'),
              dat: datJson
            };
            ajax(22);
        break;
      case 13:
          $("#Ciudad option[value="+ datos[i].CiudaT +"]").attr("selected",true);
          datos[14] = {nombre:"Select",opt:"limpiaN",centroT:datos[i].centroT};
          selector[14]=$("#centroFormacion");
          datJson = '{"estado":"3","ciudad":"'+datos[i].CiudaT+'"}';
          jso[14] = {
            url:'../../usuarios',
            token: $('meta[name="csrf-token"]').attr('content'),
            dat: datJson
          };
          ajax(14);
        break;
      case 14:
          $("#centroFormacion option[value="+ datos[i].centroT +"]").attr("selected",true);
          datos[15] = {nombre:"btn"};
          datJson = '{"estado":"9","user":"'+usuario+'"}';
          jso[15] = {
              url:'../../usuarios',
              token: $('meta[name="csrf-token"]').attr('content'),
              dat: datJson
          };
         ajax(15);
        break;
      case 15:
      var arrPruebaUsu=[];
          var jsoli = jQuery.parseJSON(data[i]);
          $("#contenedorClon").html("");
          $('#selectTipoUsuario').find('option').prop('disabled', false);
           arraySelecionPrueba=[]; arrayEliminarPrueba=[];
          for (var i = 0; i < jsoli.length; i++) {
            if (jsoli[i].tipoRol=="0") {//rol normal
              if (jsoli[i].idRol=="3") {
                Usutesting =1;
                $("#opcionesTesting").show();
              }
                $('#selectTipoUsuario option[value="'+jsoli[i].idRol+'"]').attr("disabled", true);
                arrUsuarioJ.push(jsoli[i].nombreRol);
                var clon = $("#clonTipoUsuario").clone();
                clon.find(".rowElement").attr('id','rowElement'+count);
                clon.find(".labelTipoUsuario").text(jsoli[i].nombreRol);
                clon.find(".btnELiminarTipo").attr('value','rowElement'+count+"$$"+jsoli[i].idRol);
                clon.children().appendTo($("#contenedorClon"));
                count++;
            }else{
              var id = [""+jsoli[i].idRol+""];
              arraySelecionPrueba.push(jsoli[i].idRol);
              $('#SelectIpruebas').multiSelect('select',id);
            }
          }
        break;
      case 22:
        $("#contenido").html("");
        $("#contenido").html(data[i]);
        $("#useClassModulo").attr('class','');
        $("#useClassModulo").attr('class','glyph stroked '+datos[22].icon+'');
        $("#useClass1Modulo").attr('xlink:href',datos[22].label);
        $("#titulo").text(datos[22].titulo);
       break;
    }
  };

  function ciudadfun(departamento,tP,CiudaTs,centroTs){
    var idAjax=11;
    if (tP==1) {
      idAjax=13;
      datos[idAjax] = {nombre:"Select",opt:"limpiaN",CiudaT:CiudaTs,centroT:centroTs};
    }else{
      datos[idAjax] = {nombre:"Select",opt:"limpiaN"};
    }
    selector[idAjax]=$("#Ciudad");
    datJson = '{"estado":"2","departamento":"'+departamento+'"}';
    jso[idAjax] = {
      url:'../../usuarios',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(idAjax);
  };

  function centroFormacionfun(centro){
    datos[12] = {nombre:"Select",opt:"limpiaN"};
    selector[12]=$("#centroFormacion");
    datJson = '{"estado":"3","ciudad":"'+centro+'"}';
    jso[12] = {
      url:'../../usuarios',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(12);
  };
});
