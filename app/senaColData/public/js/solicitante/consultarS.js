  $(document).ready(function(){
  $("#acordion").accordion({heightStyle: 'content'});
  var selector = [], hilo = [], jso = [], data = [],datos=[],solicitudP=0;
  var ob = new $.Luna("Solicitud");
  ob.Vivo("ConsultarSolicitud1");
  urlrelativo= ob.Relativo();
  selector[0]= $("#tablaConsultarP");
  ob.TablaEspa(selector[0]);
  datos[0] = {nombre:"ConsutarSolicitud"};
  jso[0] = {
    url:'../../ConsutarSolicitudes',
    token: $('meta[name="csrf-token"]').attr('content'),
    dat: "null"
  };
  ajax(0);

  $(document).on('click','.formMaldito',function(e){
    var form = $("#formS")[0];
    $.ajax({
      url: urlrelativo+'/editarSolicitud',
      type:'post',
      data: new FormData(form),
      contentType:false,
      cache:false,
      processData:false,
      success:function(resultSet){
        formAjax(resultSet);
      },
      error:function(){
        alert("Error fatal comuniquese con el administrador del sistema");
      }
    });
  })

  $(document).on('click','.btnConsultar',function(e){
    $("#tablaDiv").hide();
    $("#detalleBase").show();
    var arr =  this.id.split("$$");
    datos[1] = {nombre:"btn"};
    datJson = '{"solicitud":"'+arr[0]+'","estado":"0"}';
    solicitudP=arr[0];
    jso[1] = {
      url: '../../detalleSolicitudSelect',
      token: $('meta[name="csrf-token"]').attr('content'),
      dat: datJson
    };
    ajax(1);
  });

  function ajax(i) {
      hilo[i] = new Worker("js/base/worker.js");
      hilo[i].postMessage(jso[i]);
      hilo[i].onmessage = function (event) {
          data[i] = event.data;
          ob.cargarTabla(data[i],selector[i],datos[i]);
          ajaxResult(i);
          hilo[i].terminate();
      };
  };

  function formAjax(resultSe){
  var daMen = resultSe.split("$%");
  var men = $("#nomProducto").text();

  if (daMen[0] == "true") {
      estado = ("success");
      men = daMen[1]+" "+men;
  } else {
      estado = ("error");
      men = daMen[1]+" "+men;
  }
  $("#btnAgregar").prop('disabled', false);
  $.notify(men, estado);
  $("#detalleBase").hide();
  $("#tablaDiv").show();
 }

var estado =0,reporteValoracion="";
  function ajaxResult(i){
    if (i==1) {
      var json = jQuery.parseJSON(data[i]);
      $("#nomProducto").text(json[0].nombreSolicitud);
      $("#nTicked").text(json[0].ticked);
      $("#descripcionSolicitud").text(json[0].descripcionSolicitud);
      $("#pruebaSolicitud").text(json[0].nombre);
      $("#estadoSolicitud").text(json[0].nombreEstado);
      $("#fechaInicio").text(json[0].fechaSolicitud);
      $("#fechafin").text(json[0].fechaFinalizacion);
      reporteValoracion=json[0].reporteValoracion;
      //Estado 1 y 2 solo los detalles basicos
      //Estado 3 informes previos
      //Estado 4,5,6 son lo mismo en programacion se muestra detalles de evaluacio,informes previos,informes finales,plantilla informe de resultados.
      if ((json[0].idEstado ==3)||(json[0].idEstado==4)||(json[0].idEstado==5)||(json[0].idEstado==6)||(json[0].idEstado==8)) {
        selector[2]=$("#acordion");
        datos[2] = {nombre:"solicitudInformesPrevios",estado:json[0].idEstado};
        datJson = '{"solicitud":"'+solicitudP+'","estado":"1"}';
        jso[2] = {
          url: '../../detalleSolicitudSelect',
          token: $('meta[name="csrf-token"]').attr('content'),
          dat: datJson
        };
        estado=json[0].idEstado;
        ajax(2);
      };
    }else if(i==2){
      $("#acordionDiv").show();
      if ((estado >3)&&(estado <6)) {
        $("#fechafinDiv").show();
        $("#linkInformeRDIV").show();
        $("#linkInformeR").attr('href','detalleSolicitudSelect?data= {"solicitud":"'+solicitudP+'","estado":"2","solicitudEstado":"'+estado+'"}' );
      }else if ((estado==6)||(estado==8)) {
        datos[3] = {nombre:"itemSolicitudSolicitante"};
        datJson = '{"solicitud":"'+solicitudP+'","estado":"3"}';
        jso[3] = {
          url: '../../detalleSolicitudSelect',
          token: $('meta[name="csrf-token"]').attr('content'),
          dat: datJson
        };
        $("#Inconsistencias").text(reporteValoracion);
        ajax(3);
      }
    }
  }
});
