
CREATE SCHEMA IF NOT EXISTS colombiadata;
USE colombiadata;

/*Generador de informacion*/
CREATE TABLE IF NOT EXISTS  Apellido(
  idApellido INT  NOT NULL AUTO_INCREMENT,
  apellido VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (idApellido)
);
CREATE TABLE IF NOT EXISTS Nombre(
  idNombre INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(500) NOT NULL,
  PRIMARY KEY (idNombre)
);

CREATE TABLE IF NOT EXISTS Palabras(
  idPalabra INT  NOT NULL AUTO_INCREMENT,
  nomPalabra VARCHAR(500) NOT NULL,
  PRIMARY KEY (idPalabra)
);

CREATE TABLE IF NOT EXISTS Profesion(
  idProfesion INT NOT NULL AUTO_INCREMENT,
  nombreProfesion VARCHAR(500) NOT NULL,
  PRIMARY KEY (idProfesion)
);

CREATE TABLE IF NOT EXISTS Territorio(
  idTerritorio INT NOT NULL AUTO_INCREMENT,
  nombreDepartamento VARCHAR(500) NOT NULL,
  nombreCiudad VARCHAR(500) NOT NULL,
  PRIMARY KEY (idTerritorio)
);

CREATE TABLE IF NOT EXISTS Tipodato(
  idTipodato INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(75) NOT NULL,
  PRIMARY KEY (idTipodato)
);

/*Aplicacion administrativa*/
create table if not exists Departamento(
  idDepartamento INT  NOT NULL AUTO_INCREMENT,
  Nombre_Departamento VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (idDepartamento)
);

create table if not exists ciudad(
  idCiudad INT  NOT NULL AUTO_INCREMENT,
  nombreCiudad VARCHAR(50) not null,
  fkDepartamento int not null,
  INDEX fkDepartamento (fkDepartamento ASC),
  CONSTRAINT  fkDepartamento
    FOREIGN KEY (fkDepartamento)
    REFERENCES Departamento(idDepartamento),
  PRIMARY KEY (idCiudad)
);

create table if not exists CentroFormacion(
  idCentroFormacion INT  NOT NULL AUTO_INCREMENT,
  nombreCentroFormacion VARCHAR(50) NULL DEFAULT NULL,
  descripcionCentroFormacion VARCHAR(50) NULL DEFAULT NULL,
  telefonoCentroFormacion varchar(50)not null,
  fkCiudad int not null,
  INDEX fkCiudad (fkCiudad ASC),
  CONSTRAINT  fkCiudad
    FOREIGN KEY (fkCiudad)
    REFERENCES ciudad(idCiudad),
	PRIMARY KEY (idCentroFormacion)
);

CREATE TABLE IF NOT EXISTS Prueba(
  idPrueba INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  descripcion VARCHAR(500) NOT NULL,	
  estado int not null,
  PRIMARY KEY (idPrueba)
);

CREATE TABLE IF NOT EXISTS plantillasReportes(
  idPlantilla INT NOT NULL AUTO_INCREMENT,
  nombrePlantilla VARCHAR(30) NOT NULL,
  formatoPlantilla VARCHAR(500) NOT NULL,
  descripcionPlantilla varchar(60)not null,
  fechaVigencia date not null,
  estado int not null,
  fkPrueba2 INT NOT NULL,
  PRIMARY KEY (idPlantilla),
  INDEX fkPrueba2 (fkPrueba2 ASC),
  CONSTRAINT  fkPrueba2
    FOREIGN KEY (fkPrueba2)
    REFERENCES Prueba(idPrueba)
);

CREATE TABLE IF NOT EXISTS Estado(
  idEstado INT NOT NULL AUTO_INCREMENT,
  nombreEstado VARCHAR(40) NULL DEFAULT NULL,
  descripcionEstado VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (idEstado)
);
/*
	estados de usuario 
     0. usuario creado
     1. sin usar usuario con contraseña validado|
     2. usuario deshabilitado
*/


CREATE TABLE IF NOT EXISTS Usuario(
  idUsuario INT NOT NULL AUTO_INCREMENT,
  documento VARCHAR(30) NOT NULL unique,
  nombres VARCHAR(100) NOT NULL,
  apellidos VARCHAR(100) NOT NULL,
  correoElectronico VARCHAR(40) NOT NULL,
  password VARCHAR(72) NOT NULL,
  estado int not null,
  fkCentroFormacion int not null,
  PRIMARY KEY (idUsuario),
  INDEX fkcentroFormacion(fkcentroFormacion ASC),
	CONSTRAINT  fkcentroFormacion
    FOREIGN KEY (fkCentroFormacion)
    REFERENCES CentroFormacion(idCentroFormacion)
);


CREATE TABLE IF NOT EXISTS  Rol(
  idRol INT NOT NULL AUTO_INCREMENT,
  nombreRol varchar(25) not null,
  descripcion varchar (50)not null,
  tipoRol int not null,
  fkPrueba int,
    primary key(idRol)
);  

CREATE TABLE IF NOT EXISTS  Tipousuario(	
  idTipoUsuario int not null auto_increment,
  fkRol int not null,
  fkUsuario int not null,
  primary key(idTipoUsuario),
  INDEX fkRol(fkRol ASC),
  CONSTRAINT fkRol
    FOREIGN KEY (fkRol)
    REFERENCES  Rol(idRol),
     INDEX fkUsuario(fkUsuario ASC),
	CONSTRAINT  fkUsuario
    FOREIGN KEY (fkUsuario)
    REFERENCES Usuario(idUsuario)
);


CREATE TABLE IF NOT EXISTS Solicitud(
  idSolicitud INT NOT NULL AUTO_INCREMENT,
  nombreSolicitud VARCHAR(30) NOT NULL,
  descripcionSolicitud VARCHAR(500) NOT NULL,
  fechaSolicitud timestamp  DEFAULT current_timestamp,
  fechaFinalizacion timestamp  DEFAULT current_timestamp,
  ticked VARCHAR(100) NOT NULL,
  reporteValoracion varchar(500) null,
  fksolicitante INT NOT NULL,
  fkTestig INT NULL DEFAULT NULL,
  idPrueba INT NOT NULL,
  fkEstado INT NOT NULL,
  fkCentro int not null,
  PRIMARY KEY (idSolicitud),
  INDEX fkSolicitud1 (fksolicitante ASC),
  INDEX fkEstado (fkEstado ASC),
  INDEX fkPrueba (idPrueba ASC),
  CONSTRAINT fkEstado
    FOREIGN KEY (fkEstado)
    REFERENCES Estado(idEstado),
  CONSTRAINT fkPrueba
    FOREIGN KEY (idPrueba)
    REFERENCES Prueba(idPrueba),
  CONSTRAINT fkSolicitud1
    FOREIGN KEY (fksolicitante)
    REFERENCES  Usuario(idUsuario)
);

CREATE TABLE IF NOT EXISTS TipoDocumento(
	idTipoDocumento int not null auto_increment,
    tipoDocumento varchar(25)not null,
    PRIMARY KEY (idTipoDocumento)
);

/*estadoDocumento = 0 validado
					1 rechazado*/
                 
CREATE TABLE IF NOT EXISTS Documentosolicitud(
  idCertificadoSolicitud INT NOT NULL AUTO_INCREMENT,
  fktipoDocument int not null,
  fkSolicitud INT NOT NULL,
  urlArchivo varchar(500)not null,
  fechaDocumento varchar (100)NOT NULL,
  descripcionDocumento VARCHAR(500),
  estadoDocumento int not null,
  PRIMARY KEY (idCertificadoSolicitud),
  INDEX fkCertificadoSolicitud (fkSolicitud ASC),
  CONSTRAINT fkCertificadoSolicitud
    FOREIGN KEY (fkSolicitud)
    REFERENCES Solicitud(idSolicitud),
	INDEX fktipoDocument(fktipoDocument ASC),
	CONSTRAINT fktipoDocument1
    FOREIGN KEY (fktipoDocument)
    REFERENCES TipoDocumento(idTipoDocumento)
  
);

CREATE TABLE IF NOT EXISTS RequisitosPrueba(
  idItem INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(100) NOT NULL,
  descripcion VARCHAR(500) NOT NULL,
  fkPrueba INT(11) NOT NULL,
  PRIMARY KEY (idItem),
  INDEX fkitemPrueba22 (fkPrueba ASC),
  CONSTRAINT fkitemPrueba22
    FOREIGN KEY (fkPrueba)
    REFERENCES Prueba(idPrueba)
);

CREATE TABLE IF NOT EXISTS Evaluacionsolicitud(
  idEvaluacionSolicitud INT NOT NULL,
  valoracion varchar(10) NOT NULL, 
  observacion VARCHAR(500) NOT NULL,
  fkSolicitud INT NOT NULL,
  fkItemPrueba INT NOT NULL,
  INDEX fkDetallesolicitudItem (fkItemPrueba ASC),
  CONSTRAINT fkDetallesolicitudItem
    FOREIGN KEY (fkItemPrueba)
    REFERENCES RequisitosPrueba(idItem)
);

CREATE TABLE IF NOT EXISTS Herramienta(
  idHerramienta INT NOT NULL AUTO_INCREMENT,
  nombreHerramienta VARCHAR(30) NOT NULL,
  descripcionHerramienta VARCHAR(500) NOT NULL,
  urlHerramienta VARCHAR(500) NOT NULL,
  PRIMARY KEY (idHerramienta)
);

CREATE TABLE IF NOT EXISTS Heramientamp(
  idherramientaMP INT NOT NULL AUTO_INCREMENT,
  idPrueba INT NOT NULL,
  idHerramienta INT NOT NULL,
  PRIMARY KEY (idherramientaMP),
  INDEX idHerramienta (idHerramienta ASC),
  CONSTRAINT idHerramienta
    FOREIGN KEY (idHerramienta)
    REFERENCES Herramienta(idHerramienta),
  INDEX idPrueba (idPrueba ASC),
  CONSTRAINT idPrueba
    FOREIGN KEY (idPrueba)
    REFERENCES Prueba(idPrueba)
);

create table tokenUser(
	idToken int not null auto_increment,
    numeroToken varchar(72) not null,
    fecha date not null,
    documento varchar (30)not null,
    primary key(idToken)
);

SET GLOBAL event_scheduler = ON;
/*
CREATE EVENT solicitudCancelada 
  ON SCHEDULE 
    EVERY 1 DAY STARTS '2017-05-09 16:39:01'
  DO
    UPDATE Solicitud
    SET fkEstado = 5
    where fechaFinalizacion < curdate()*/
    
    
DROP PROCEDURE IF EXISTS sessionC; 
 DELIMITER $$
 CREATE PROCEDURE sessionC(
	IN user varchar(30)
	)
	BEGIN
		select t1.idUsuario,t1.nombres,t1.apellidos,t1.fkCentroFormacion,t3.idRol 
		from Usuario t1
		inner join Tipousuario t2 on t2.fkUsuario = t1.idUsuario
		inner join Rol t3 on t2.fkRol = t3.idRol
		where  (t1.documento=user and t3.tipoRol=0 and t1.estado=1);
		END 
  $$



DROP PROCEDURE IF EXISTS verificarSolicitudes; 
 DELIMITER $$
 CREATE PROCEDURE verificarSolicitudes(
	IN centro INT	
	)
	BEGIN
	select  t1.idSolicitud,t1.idPrueba as idP,t1.nombreSolicitud,
			t1.descripcionSolicitud,t1.ticked,
            t2.nombre as prueba,t1.fechaSolicitud
		from Solicitud t1
		inner join Prueba t2 on t1.idPrueba=t2.idPrueba
		where fkCentro=centro and fkEstado=1
		ORDER BY fechaSolicitud asc;
		END 
  $$

DROP PROCEDURE IF EXISTS SolicitudTestinging; 
 DELIMITER $$
 CREATE PROCEDURE SolicitudTestinging(
	IN test INT	
	)
	BEGIN
		select  idSolicitud,nombreSolicitud from Solicitud 
		where fkTestig=test and (fkEstado = 2 or fkEstado = 3);
	END 
  $$ 
  
DROP PROCEDURE IF EXISTS requerimientoSolicitud; 
	 DELIMITER $$
	 CREATE PROCEDURE requerimientoSolicitud(
		IN Solicitud INT	
		)
		BEGIN
			select idCertificadoSolicitud as documento,descripcionDocumento,urlArchivo 
			from Documentosolicitud
			where (fktipoDocument =1 or fktipoDocument =4)  and fkSolicitud=Solicitud;
		END 
	  $$


DROP PROCEDURE IF EXISTS conItemsRequisitos; 
 DELIMITER $$
 CREATE PROCEDURE conItemsRequisitos(
	IN prueba INT	
	)
	BEGIN
		select idItem,nombre
		from RequisitosPrueba 
		where fkPrueba=prueba;
	END 
  $$


DROP PROCEDURE IF EXISTS pruebaSelect; 
 DELIMITER $$
 CREATE PROCEDURE pruebaSelect(
	IN pruebaSelect INT	
	)
	BEGIN
	  select  t1.idUsuario,concat(t1.nombres,' ',t1.apellidos) as nombre 
	  from Usuario t1
	  inner join Tipousuario t2 on t2.fkUsuario =t1.idUsuario
      inner join Rol t3 on t3.idRol=t2.fkRol
	  where (t3.fkPrueba=pruebaSelect and t3.tipoRol=1);
	END 
  $$

 call pruebaSelect(1)
DROP PROCEDURE IF EXISTS solicitudesTesting; 
 DELIMITER $$
 CREATE PROCEDURE solicitudesTesting(
	IN selectTesting INT,
	in estado int
	)
  BEGIN 
	select t1.idSolicitud,t2.idPrueba as idP,
		   t1.nombreSolicitud,t1.descripcionSolicitud,
           t1.ticked,
           t2.nombre as prueba,t2.descripcion as descripcionPrueba,
           t1.fechaSolicitud 
	from Solicitud t1
	inner join Prueba t2 on t2.idPrueba=t1.idPrueba
	where fkTestig = selectTesting and fkEstado=estado;
  END 
  $$

DROP PROCEDURE IF EXISTS herramientasUrl; 
 DELIMITER $$
 CREATE PROCEDURE herramientasUrl(
	IN prueba INT
	)
  BEGIN
	select nombreHerramienta,descripcionHerramienta,urlHerramienta
	from Herramienta t1
    inner join Heramientamp t2 on t1.idHerramienta=t2.idHerramienta
    where t2.idPrueba= prueba;
   END 
  $$

DROP PROCEDURE IF EXISTS detallesConsultaSoliB; 
 DELIMITER $$
 CREATE PROCEDURE detallesConsultaSoliB(
	IN solicitante INT,
    IN idSolicitud INT
	)
  BEGIN
  select t1.idSolicitud,t1.nombreSolicitud,t1.descripcionSolicitud,t1.reporteValoracion,
  t1.fechaSolicitud,t1.fechaFinalizacion,t1.ticked,t2.idEstado,
  nombreEstado,t3.idPrueba,t3.nombre
  from Solicitud  t1
  inner join Estado t2 on t1.fkEstado=t2.idEstado
  inner join Prueba t3 on t1.idPrueba = t3.idPrueba	
  where   fksolicitante=solicitante and t1.idSolicitud=idSolicitud;
   END 
  $$

DROP PROCEDURE IF EXISTS informesT; 
 DELIMITER $$
 CREATE PROCEDURE informesT(
	IN idSolicitud INT
	)
  BEGIN
	select urlArchivo, DATE_FORMAT(fechaDocumento,"%d/%m/%Y") as fechaDocumento,descripcionDocumento,fktipoDocument
    from Documentosolicitud
	where fkSolicitud= idSolicitud and (fktipoDocument =5 or fktipoDocument=2);
 END 
$$
  
DROP PROCEDURE IF EXISTS solicitudPdf; 
 DELIMITER $$
 CREATE PROCEDURE solicitudPdf(
	IN solicitud INT	
	)
  BEGIN
	 select t1.nombreSolicitud,t1.fechaSolicitud,
			t1.fechaFinalizacion,t2.nombreEstado,t3.nombre as prueba,
			t4.nombres,t4.apellidos,
            t5.nombres as testingNo,t5.apellidos as testingApe
            
	 from Solicitud t1
	 inner join Estado t2 on t2.idEstado = t1.fkEstado
	 inner join Prueba t3 on t3.idPrueba = t3.idPrueba
	 inner join Usuario t4 on t4.idUsuario = t1.fksolicitante
	 inner join Usuario t5 on t5.idUsuario=  t1.fkTestig 
	 where t1.idSolicitud=solicitud 
     GROUP BY t1.idSolicitud  ASC;
 END     
$$

DROP PROCEDURE IF EXISTS herramientaPrueba; 
 DELIMITER $$
 CREATE PROCEDURE herramientaPrueba(
	IN prueba INT	
	)
  BEGIN
	select t2.idHerramienta,t3.nombreHerramienta as nombre
	from prueba t1 
	inner join  Heramientamp  t2  on t1.idPrueba = t2.idPrueba 
    inner join herramienta t3 on  t2.idHerramienta= t3.idHerramienta
	where t1.idPrueba=prueba; 
 END     
$$

DROP PROCEDURE IF EXISTS consultaModificarUsuario; 
 DELIMITER $$
 CREATE PROCEDURE consultaModificarUsuario(
	IN usuario INT	
	)
  BEGIN
	select t1.idRol,t1.nombreRol,t1.tipoRol,fkPrueba from rol t1 
	inner join tipousuario t2 on  t2.fkRol=t1.idRol
	inner join usuario t3 on t3.idUsuario = t2.fkUsuario
	where t3.idUsuario=usuario;
 END     
$$

DROP PROCEDURE IF EXISTS cambiarEstadoUsuario; 
 DELIMITER $$
 CREATE PROCEDURE cambiarEstadoUsuario(
	IN documeto varchar(30),
	IN con varchar(72))
    begin
		update usuario set
		password= con ,
		estado = 1  
		where documento = documeto;
 END     
$$


create view userTable 
as 
select t1.idUsuario as usuario,t1.nombres,t1.apellidos,t1.correoElectronico,t1.documento,t1.estado,
	t2.idCentroFormacion as centro,t3.idCiudad as ciudad ,t4.idDepartamento as departamento
	from Usuario t1 inner join CentroFormacion t2 on t1.fkCentroFormacion = t2.idCentroFormacion
    inner join ciudad t3 on t2.fkCiudad= t3.idCiudad
    inner join Departamento t4 on t3.fkDepartamento=t4.idDepartamento; 

/*Vistas del aplicativo
    
insert into Tipodato values (0,'Ciudad');
insert into Tipodato values (0,'Departamento');
insert into Tipodato values (0,'Departamento/Ciudad');
insert into Tipodato values (0,'Profesion');
insert into Tipodato values (0,'Nombres');
insert into Tipodato values (0,'Apellidos');
insert into Tipodato values (0,'Cedula');
insert into Tipodato values (0,'Telefono celular');
insert into Tipodato values (0,'Telefono fijo');
insert into Tipodato values (0,'Contraseña');
insert into Tipodato values (0,'Direccion');
insert into Tipodato values (0,'Descripcion');
insert into Tipodato values (0,'Intervalo');
insert into Tipodato values (0,'Varchar');
insert into Tipodato values (0,'Int');
insert into Tipodato values (0,'Decimales');
insert into Tipodato values (0,'Porcentajes');
insert into Tipodato values (0,'AutoIncrementable');
insert into Tipodato values (0,'Boolean');
insert into Tipodato values (0,'Date Y-M-D');
insert into Tipodato values (0,'Date M-D-Y');
insert into Tipodato values (0,'Date D-M-Y');
insert into Tipodato values (0,'Fechas Anteriores formato: Y-M-D');
insert into Tipodato values (0,'Fechas posterioes formato: Y-M-D');
 
insert into Rol values(0,'Solicitante','Creador de las solicitudes.',0,0);
insert into Rol values(0,'Gestionador','Getiona  las solicitudes del sistema.',0,0);
insert into Rol values(0,'Testing','Realiza las pruebas del sistema.',0,0);
insert into Rol values(0,'Administrador','Realiza mantenimiento al sistema.',0,0);
insert into Prueba values(0,'Caja blanca','Descripcion de la prueba caja blanca',0);

INSERT INTO Departamento (Nombre_Departamento)VALUES ('Amazonas');
INSERT INTO Departamento (Nombre_Departamento)VALUES ('Antioquia');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Arauca');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Atlántico');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Bogotá D.C.');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Bolivar');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Boyacá');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Caldas');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Caquetá');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Casanare');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Cauca');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Cesar');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Choco');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Córdoba');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Cundinamarca');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Guainia');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Guaviare');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Huila');
INSERT INTO Departamento (Nombre_Departamento)VALUES('La Guajira');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Magdalena');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Meta');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Nariño');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Norte de Santander');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Putumayo');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Quindio');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Risaralda');
INSERT INTO Departamento (Nombre_Departamento)VALUES('San Andres y Providencia');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Santander');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Sucre');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Tolima');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Valle del Cauca');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Vaupes');
INSERT INTO Departamento (Nombre_Departamento)VALUES('Vichada');

LOAD DATA LOCAL INFILE '/Codigos.csv'INTO TABLE ciudad
FIELDS TERMINATED BY ';'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

insert into CentroFormacion values(0,'Centro de Gestión de Mercados, Logística y Tecnologías de la información','DesCentro','5753494',167);

insert into Usuario values(0,'1026297371','Juan Andres','Lopez Alvarez','jalopez173@misena.edu.co','$2y$10$.oscR0w7oEMkUUiVk7LPp.cVSh6T2zT/4shFsX84f5jV7krITGSu.',1,1);
insert into Usuario values(0,'2026297371','Miguel  Angel','castiblanco','miguelAngel@misena.edu.co','$2y$10$Ovhb1ajWrTYeeZvhpMMpUuF.IhRMFRDHZr7fX957hvcAjftdUTFgC',1,1);
insert into Usuario values(0,'3026297371','Mayra Alejandra','Muñoz Pineda','may@misena.edu.co','$2y$10$q2X2nXCbdYPteh1qcJ1Otejy9zd9nSFa8/0PzK8WfMzuzQ8yyMELW',1,1);
insert into Usuario values(0,'4026297371','Maicol' ,'ospina rojas','maicol@misena.edu.co','$2y$10$czi.XamlXlRzi./ArjK2L.E4QGMeDmZAno9dCAsr7wtRn00Ye5FOW',1,1);
insert into Usuario values(0,'5026297371','Judini','Cambell','JudiniCambell@misena.edu.co','$2y$10$PtCJPc9Lhiv00Y.EtrhnguNvW6gd0iAtI9IKaE2e6dPCHo/BtvHrq',1,1);

insert into Tipousuario values(0,1,1);
insert into Tipousuario values(0,2,2);
insert into Tipousuario values(0,3,3);
insert into Tipousuario values(0,3,4);
insert into Tipousuario values(0,4,5);	

insert into Rol values(0,'Caja blanca','Autorizado para hacer pruebas de caja blanca',1,1);
insert into Tipousuario values(0,5,3);
insert into Tipousuario values(0,5,4);
insert into Herramienta	values(0,'Generador de informacion','Crea informacion parametrizable en formato csv','http://localhost/proyectos/Dinamico/blog/public/archivo');
insert into RequisitosPrueba value(0,'validaciones de campo','Los campos de la vista html contiene las validaciones de tipo de dato',1);
insert into RequisitosPrueba value(0,'Estructura del codigo','El codigo tiene una estrutura escalable y entendible',1); 
insert into RequisitosPrueba value(0,'Estructura','El codigo tiene una estrutura escalable y entendible',1); 
insert into Heramientamp values(0,1,1);
insert into Estado values(0,'En espera de revision','En espera de revision');
insert into Estado values(0,'Testing asignado','Se le asigno un tester');
insert into Estado values(0,'Realizando pruebas','se estan realiza las pruebas');
insert into Estado values(0,'Aprobado','La solicitud  a aprobado');
insert into Estado values(0,'Reprobado','La solicitud a reprobado');
insert into Estado values(0,'Devolucion gestionador','La solicitud se devuelve por alguna inconsistencia');
insert into Estado values(0,'Cancelada','La solicitud se a cancelado');
insert into Estado values(0,'Devolucion experto','La solicitud se devuelve por alguna inconsistencia');
insert into TipoDocumento values(0,'Documentos requeridos');
insert into TipoDocumento values(0,'Resultados parciales');
insert into TipoDocumento values(0,'Certificados');
insert into TipoDocumento values(0,'Elemento de la solicitud');
insert into TipoDocumento values(0,'Calificacion final');
insert into plantillasReportes values(0,'Plantilla1','plantilla1.docx','des plantilla1','2017-06-30',0,1);
insert into plantillasReportes values(0,'Plantilla2','plantilla2.docx','des plantilla 2','2017-06-30',0,1);

*/
